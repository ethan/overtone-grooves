(ns jams.jam-20210202-01
  (:use
   [overtone.core])
  (:require
   [fastmath.core :as m]
   [score.core :as s]))

(connect-external-server)
(require 'overtone.inst.drum :refer [kick])
(use 'overtone.inst.synth)
(use 'groove-step-seq.groove-buffer)
(groove-buffer-synth-init)

(kick)
(def MAX-PARTIALS 30)
(defsynth adtv-tri-synth [out-bus 0 freq 55 amp 0.5 gate 1 partials MAX-PARTIALS phase-spread 1]
  (let [waves (map (fn [i] (let [partial (+ 1 (* 2 i))
                                 osc-freq (* partial freq)
                                 osc-phase (* partial m/PI phase-spread)
                                 osc-amp (/ amp (* partial partial))
                                 osc-amp (* osc-amp (min 1.0 (max 0 (- partials i))))]
                             (if (< i partials)
                               (* osc-amp (sin-osc:ar osc-freq osc-phase))
                               0)))
                   (range 0 MAX-PARTIALS))
        wave (reduce + waves)]
    (out:ar out-bus (pan2 wave))))

(comment
  (def b (adtv-tri-synth :freq 32 :partials 3))
  (ctl b :partials 20)
  (ctl b :amp 0.1)
  (ctl b :phase-spread 1)
  (ctl b :freq 44.0)
  (stop))

(defsynth adtv-sqr-synth [out-bus 0 freq 55 amp 0.5 gate 1 partials MAX-PARTIALS phase-spread 1]
  (let [waves (map (fn [i] (let [partial (+ 1 (* 2 i))
                                 osc-freq (* partial freq)
                                 osc-phase 0
                                 osc-amp (/ amp partial)
                                 osc-amp (* osc-amp (min 1.0 (max 0 (- partials i))))]
                             (if (< i partials)
                               (* osc-amp (sin-osc:ar osc-freq osc-phase))
                               0)))
                   (range 0 MAX-PARTIALS))
        wave (reduce + waves)]
    (out:ar out-bus (pan2 wave))))

(comment
  (def s (adtv-sqr-synth))
  (ctl s :freq 44)
  (ctl s :partials 4.5)
  (stop))

(defsynth adtv-saw-synth [out-bus 0 freq 55 amp 0.5 gate 1 partials MAX-PARTIALS phase-spread 1]
  (let [waves (map (fn [i] (let [partial (+ 1 i)
                                 osc-freq (* partial freq)
                                 osc-phase 0
                                 osc-amp (/ amp partial)
                                 osc-amp (* osc-amp (min 1.0 (max 0 (- partials i))))]
                             (if (< i partials)
                               (* osc-amp (sin-osc:ar osc-freq osc-phase))
                               0)))
                   (range 0 MAX-PARTIALS))
        wave (reduce + waves)]
    (out:ar out-bus (pan2 wave))))

(comment
  (def saw-test (adtv-saw-synth))
  (ctl saw-test :partials 30)
  (ctl saw-test :amp 0.15)
  (ctl saw-test :freq 50)
  (stop))

;; TODO figure out how to DRY up adtv code with a macro or similar
;; (defmacro adtv-osc [fund-freq & {:keys [num-partials partial-fn freq-fn phase-fn amp-fn]
;;                              :or {num-partials MAX-PARTIALS,
;;                                   partial-fn (partial + 1),
;;                                   freq-fn (fn [p i] (mul-add fund-freq p 0))
;;                                   phase-fn (fn [& rest] 0),
;;                                   amp-fn (fn [& rest] 1)}}]
;;   '(let [waves (map (fn [i] (let [p (~partial-fn i)
;;                                  osc-freq (freq-fn fund-freq p)
;;                                  osc-phase (phase-fn p i)
;;                                  osc-amp (amp-fn p i)
;;                                  osc-amp (* osc-amp (min 1.0 (max 0 (- num-partials i))))]
;;                              (if (< i num-partials)
;;                                (* osc-amp (sin-osc:ar osc-freq osc-phase))
;;                                0)))
;;                    (range 0 MAX-PARTIALS))]
;;     (reduce + waves)))

;; (macroexpand '(adtv-osc 20))

;; (defsynth adtv-saw-synth2 [out-bus 0 freq 55 amp 0.5 gate 1 partials MAX-PARTIALS phase-spread 1]
;;   (let [wave (adtv-osc freq :freq-fn * :amp-fn (fn [n & rest] (/ 1 n)))
;;         wave (* wave amp)]
;;     (out:ar out-bus (pan2 wave))))

(defsynth adtv-saw-synth [out-bus 0 freq 55 amp 0.5 gate 1 partials MAX-PARTIALS phase-spread 1]
  (let [waves (map (fn [i] (let [partial (+ 1 i)
                                 osc-freq (* partial freq)
                                 osc-phase 0
                                 osc-amp (/ amp partial)
                                 osc-amp (* osc-amp (min 1.0 (max 0 (- partials i))))]
                             (if (< i partials)
                               (* osc-amp (sin-osc:ar osc-freq osc-phase))
                               0)))
                   (range 0 MAX-PARTIALS))
        wave (reduce + waves)]
    (out:ar out-bus (pan2 wave))))


;; Adapted from: https://forum.cockos.com/showthread.php?t=107916
;; // Rectangular wave
;; pw = $pi * 0.25; // 25% duty cycle
;; loop(...
;;   s += cos(n * t) * sin(n * pw) / n;
;;   n += 1;
;; );
;; s *= 4/$pi;
;; TODO Figure out what the 4/PI corresponds to here (guessing every other)


(defsynth adtv-rect-synth [out-bus 0 freq 55 amp 0.5 duty 0.5 gate 1 partials MAX-PARTIALS phase-spread 1]
  (let [waves (map (fn [i] (let [partial (+ 1 (* 2 i))
                                 osc-freq (* partial freq)
                                 osc-amp (/ amp partial)
                                 osc-amp (* (sin (* m/PI partial duty)) osc-amp)
                                 osc-amp (* osc-amp (min 1.0 (max 0 (- partials i))))]
                             (if (< i partials)
                               (* osc-amp
                                  (sin-osc:ar osc-freq m/HALF_PI))
                               0)))
                   (range 0 MAX-PARTIALS))
        wave (reduce + waves)]
    (out:ar out-bus (pan2 wave))))

(comment
  (def p1 (adtv-rect-synth))
 (ctl p1 :duty 0.01)
 (ctl p1 :partials 10)
 (ctl p1 :freq (note :D1))
 (kill p1)
 )

(recording-start "~/audio/recordings/2021-02-09--jam-glitch-verb.wav")
(recording-stop)

;; TODO: supersaw, gapped saw, adtv tri drum, and adtv fm

(definst adtv-tri-kick [out-bus 0 freq 32 amp 0.5 click-decay 0.007 click-amt 10 freq-decay 0.05 freq-env-amt 1000 amp-decay 0.5 gate 1 partials MAX-PARTIALS phase-spread 1]
  (let [amp-env (env-gen:ar
                 (env-perc 0.01 amp-decay)
                 :action FREE)
        freq-env (env-gen:ar
                  (env-perc 0.01 freq-decay))
        click-env (env-gen:ar
                   (env-perc 0.0001 click-decay))
        freq-env (mul-add:ar click-env click-amt freq-env)
        freq (mul-add:ar freq-env freq-env-amt freq)
        waves (map (fn [i] (let [partial (+ 1 (* 2 i))
                                 osc-freq (* partial freq)
                                 osc-phase (* partial m/PI phase-spread)
                                 osc-amp (/ amp (* partial partial))
                                 osc-amp (* osc-amp (min 1.0 (max 0 (- partials i))))]
                             (if (< i partials)
                               (* osc-amp (sin-osc:ar osc-freq osc-phase))
                               0)))
                   (range 0 MAX-PARTIALS))
        wave (reduce + waves)]
    ;; (out:ar out-bus (pan2 (* amp-env wave)))
    (* amp-env wave)))

(adtv-tri-kick :freq (midi->hz (note :D1)) :freq-env-amt 200 :freq-decay 0.1 :amp-decay 0.8 :partials 30 :amp 0.5)

(def groove1 (groove-metro-synth server-clock-b 112 4 4 1))
(def groove1-buffer-a (generate-groove-buffer
                       (make-interpolation-fn m/pow2) 112 16))
(write-groove-buffer groove1-buffer-a
                     (make-interpolation-fn m/pow2 :interpolation-amount 0.0)
                     112)
(def groove1-grooverA (add-groove-transform-synth groove1 groove-clock-shaper-stepped groove1-buffer-a))

(def state (atom {:groove1a-amt 1.0 :groove2a-amt 0.25 :groove1a-fn m/pow2 :groove2a-fn m/pow3}))
(swap! state assoc :groove1a-amt 0.02)
(swap! state assoc :groove2a-amt 0.02)
(swap! state assoc :groove2b-amt 0.03)
(swap! state assoc :groove1a-fn #(m/safe-sqrt %))
(swap! state assoc :groove2a-fn #(m/pow2 %))
(swap! state assoc :groove2a-fn m/pow2)
(swap! state assoc :kick-pattern #{0})
(swap! state assoc :kick-pattern #{0 4 8 12})
(swap! state assoc :bing-pattern #{0 2 4 6 8 10 12 14})
(defn play-kick? [step]
  (contains? (:kick-pattern @state) (int (rem step 16)))
  )
(defn play-bing? [step]
  (contains? (:bing-pattern @state) (int (rem step 16)))
  )

(defn reduce-pattern-sets [& patterns]
  "Combine a set of patterns defines as trigger step numbers"
  (set (reduce concat
               (map
                (fn [start set]
                  (map #(+ (* 16 start) %) set))
                (range) patterns))))

(swap! state assoc :kick-pattern
       (reduce-pattern-sets
        [0 1 2 3 4 6 8]
        [6 10]
        [10]
        [0 10 14]))

(defn play-kick [freq-note partials amp]
  (adtv-tri-kick :freq (midi->hz (note freq-note)) :freq-env-amt 150 :freq-decay 0.1 :amp-decay 0.8 :partials 10 :amp 0.7 :click-amt 50 :click-decay 0.015))

;; (inst-fx! adtv-tri-kick fx-compressor)
(def kick-verb (inst-fx! adtv-tri-kick fx-freeverb))
(ctl kick-verb :room-size 0.4)
(ctl kick-verb :dampening 0.1)
;; (inst-fx! adtv-tri-kick fx-compressor)
(clear-fx adtv-tri-kick)

(do 
  ;; (def combtimes-ms [50 36 11.5 7 2])
  (def combgains [-0.7 -0.5 -0.3 -0.2 -0.2])
  ;; (def alltimes-ms [4.2 3.2 1.3 5.2])
  (def combtimes-ms [53 32 67.3 127])
  (def alltimes-ms [12.2 9.9 21])
  (clear-fx adtv-tri-kick)
  (clear-fx bing)
  (clear-fx closed-hat2)
  (defsynth fx-reverb-diy1
    "Uses simple comb / allpass network"
    [bus 0 wet 0.5 dry 1 room-size 2.0 dampening 0.5]
    (let [source (in bus)
          comb-outs (mapv #(* %2 (comb-l:ar source 1.0 (/ %1 1000) (* room-size (/ 1 %3)))) combtimes-ms combgains (range 1 6))
          comb-sum (apply + comb-outs)
          allpass-in (+ (* 0.4 source) comb-sum)
          allpass-series-out (reduce #(allpass-l:ar %1 1.0 (/ %2 1000) 0.2) allpass-in alltimes-ms)
          outsig (+ (* dry source) (* wet allpass-series-out))
          ]
      
      (replace-out bus (* 1.4 outsig))))
  (def kick-verb (inst-fx! adtv-tri-kick fx-reverb-diy1))
  (def bing-verb (inst-fx! bing fx-reverb-diy1))
  (def hat-verb (inst-fx! closed-hat2 fx-reverb-diy1)))
(clear-fx adtv-tri-kick)

(def drone (adtv-sqr-synth :freq (midi->hz (note :A2))))
(ctl drone :amp 0.2)
(ctl drone :partials 30)
(ctl drone :freq (midi->hz (note :D1)))
(kill drone)

(def bing-verb (inst-fx! bing fx-reverb-diy1))
(def hat-verb (inst-fx! closed-hat2 fx-reverb-diy1))

(defn clock1-step [step]
  (if (play-kick? step)    ;; (dance-kick)
    (play-kick
     (if (> (rem step 64) 31) :D1 :C1)
     7
     0.5))
  ;; (if (= (rem step 4) 2.0) (closed-hat2)) 
  ;; (if (play-bing? step) (bing))
  ;; (if (= 0.0 (rem step 2)) (closed-hat2))
  ;; (if (= 3.0 (rem step 8))
  ;;   (noise-snare)
  ;;   ;; (play-snare-sample snare-buffer)
  ;;   )
  ;; ;; (if (= 0.0 (rem step 16)) (mapv #(overpad % :amp 0.25) [68 56 65]))
  )

;; TODO Figure out why clock 2 is skipping all over the place
(defn clock2-step [step]
  ;; (if (contains? (:bing-pattern @state) (int (rem step 16))) (bing))
  ;; (if (= 4.0 (rem step 8)) (mapv #(overpad % :amp 0.25) [49 56 61]))
  ;; (if (contains? (:kick-pattern @state) (int (rem step 64))) (dance-kick))
  ;; (if (= 0.0 (rem step 1)) (closed-hat2 :amp (/ 0.15 (+ 1 (rem step 3)))))
  (if (= 4.0 (rem step 8)) (noise-snare))
  ;; (case (rem step 4)
  ;;   2.0 (bass (midi->hz 37))
  ;;   (bass (midi->hz 25))
  ;;   )
  )

(volume 1.0)

(on-event "/clock-tick"
          ;; #(println "/clock-tick" %)
          (fn [{:keys [args path]}]
            ;; (println args)
            (let [[num clock-id step] args]
              (cond
                (= clock-id 1) (clock1-step step)
                (= clock-id 2) (clock2-step step))))

          ::clock-tick)
(remove-event-handler ::clock-tick)

(definst mic [amp 1]
  (let [src (in (num-output-buses:ir))]
    (* amp src)))
(mic)
