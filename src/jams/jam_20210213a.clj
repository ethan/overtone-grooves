(ns jams.jam-20210213a
  (:use
   [overtone.core]
   [pattern-tools.core]
   [break-sampler.core]
   )
  (:require
   [fastmath.core :as m]
   [fastmath.interpolation :as m-i]
   [fastmath.easings :as m-e]
   [score.core :as s]
   [clojure.string :as str]
   )
  )

(connect-external-server)
(use 'groove-step-seq.groove-buffer)
(groove-buffer-synth-init)

(def break (load-sample "~/audio/soundfiles/breaks/24940__vexst__the-winstons-amen-brother-full-solo-4-bars.wav"))

(do 
  (def groove1 (groove-metro-synth server-clock-b 142 4 4 1))
  (def groove1-buffer-a (generate-groove-buffer
                         (make-interpolation-fn m/pow2) 142 0x40))
  (write-groove-buffer groove1-buffer-a
                       (make-interpolation-fn m/pow2 :interpolation-amount 0.1)
                       142)
  (comment
    (def groove1-grooverA (add-groove-transform-synth groove1 groove-clock-shaper-stepped groove1-buffer-a))
    (kill groove1-grooverA)
    (kill-groove-synth groove1)
    ))

(do 
  ;; TODO move these all to a ModularDelay structure with individual outputs for
  ;; all channels.
  ;; TODO use an external mixer instead of overwriting a single output bus
  ;; Set up buffer for delay
  (def delay-1-buf (buffer (* 4 (server-sample-rate)) 2))
  ;; Set up a group for the delay
  (def delay-group (group "delay 1" :head (foundation-safe-post-default-group)))
  ;; Set up input, output, and feedback bus
  (def delay-in-bus (audio-bus 2 "delay-in"))
  (def delay-out-bus (audio-bus 2 "delay out"))
  (def delay-feedback-bus (audio-bus 2 "delay fdbk"))
  (def delay-index-bus (audio-bus 1 "delay index"))

  ;; Delay head takes a signal for location, input, and feedback and writes the
  ;; result to the buffer
  (defsynth delay-phasor [out-bus 0 max-val (server-sample-rate)]
    (out:ar out-bus (phasor:ar 0 1 0 max-val))
    )
  ;; Delay taps take a location that has been offset by some amount, read the
  ;; buffer at that position, then output the read amount * out-amp-amt to the out
  ;; chan, and read val * feedback-amp-amount to the feedback channel
  ;; TODO add delay in amt so it's easy to cut the input (e.g. latch loop)
  (defsynth delay-write [in-bus 0 feedback-bus 2 out-bus 0 index-bus 0 delay-buf 0 out-amp 1.0]
    (let [in-sigs (in:ar in-bus 2)
          feedback-sigs (in-feedback:ar feedback-bus 2)
          write-sigs (+ in-sigs feedback-sigs)
          out-sigs (* in-sigs out-amp)
          index (in:ar index-bus 1)
          ]
      (buf-wr:ar write-sigs delay-buf index)
      (replace-out:ar out-bus out-sigs)
      )
    )

  ;; Then we might add things like offseters, lfos, etc. to the signal controlling
  ;; one or more tap locations
  (defsynth delay-tap [delay-buf 0 index-bus 0 index-offset 48000 out-amp 1 out-bus 0 feedback-amp 0 feedback-bus 0]
    (let [index (- (in:ar index-bus 1) index-offset)
          index (wrap:ar index 0 (buf-frames:kr delay-buf))
          read-sigs (buf-rd:ar 2 delay-buf index)
          out-sig (* read-sigs out-amp)
          feedback-sig (* read-sigs feedback-amp)
          ]
      (out:ar feedback-bus feedback-sig)
      (out:ar out-bus out-sig)
      )
    )

  (defn beats->samps [beats bpm]
    (* (server-sample-rate) (/ 60 bpm) beats)
    )

  (def delay-1-phasor (delay-phasor [:head delay-group] delay-index-bus (buffer-size delay-1-buf)))
  (def delay-1-write (delay-write [:tail delay-group] 0 delay-feedback-bus 0 delay-index-bus delay-1-buf 1))
  (comment
    (ctl delay-1-write :out-amp 1)
    (kill delay-1-write))

  (def delay-1-tap-1 (delay-tap [:after delay-1-write] delay-1-buf delay-index-bus (* (server-sample-rate) (/ 60 142) 0.25) 0.5 0 0.5 delay-feedback-bus))
  (comment (ctl delay-1-tap-1 :out-amp 0.75 :feedback-amp 0.25)
           (ctl delay-1-tap-1 :index-offset (beats->samps 1.5 142))
           (ctl delay-1-tap-1 :index-offset 100)
           (kill delay-1-tap-1))

  (def delay-1-tap-2 (delay-tap [:after delay-1-write] delay-1-buf delay-index-bus (beats->samps 1 142) 0.5 0 0.0 delay-feedback-bus))
  (comment 
    (ctl delay-1-tap-2 :out-amp 0.75 :feedback-amp 0.5)
    (ctl delay-1-tap-2 :index-offset (beats->samps 2.5 142))
    (ctl delay-1-tap-2 :index-offset 100)
    (ctl delay-1-tap-2 :out-amp 0 :feedback-amp 0)
    (ctl delay-1-tap-2 :out-amp 0 :feedback-amp 0.5)
    (kill delay-1-tap-2)
    (pp-node-tree)))

(def state (atom {
                  :step-fns {
                              1 {}
                             }
                  :watch-fns '()
                  }))

;; DEBUG NOTES

(comment

  This should work by looping over all the functions in the step-fns key for the clock id provided.

  It's now clearly trying to run something, which is good. But errors out even on a simple println.

  It's uncldear 
  )


(defn clock1-step [step]
  (let [pattern (:current-pattern @state)]
    ;; (println step)
    (run! (fn [[key func]] (func step)) ((:step-fns @state) 1))
    ;; (play-sample-step break (nth pattern (rem step 0x40)) 0x40)
    ;; (apply (nth (:delay-set-fns @state) (rem (quot step 8) 4)) [])
    ;; (if (< (rem step 8) 6)
    ;; (apply (nth (:delay-set-fns @state) 3) [])
    ;; (apply (nth (:delay-set-fns @state) 1) []))
    )
  ;; (println (nth pattern (rem step 16)))
  )

(defn add-step-fn [clock-id key func]
  (swap! state assoc-in [:step-fns clock-id key] func)
  )

(defn remove-step-fn [clock-id key]
  (swap! state update-in [:step-fns clock-id] dissoc key)
  )

(swap! state (fn [state key func] (assoc state :step-fns (assoc (:step-fns state) key func))))
       :play-sample-fn
       )

(add-step-fn 1 :debug println)
(remove-step-fn 1 :debug)

(add-step-fn 1 :play-break (fn [step] (play-sample-step break (nth (:current-pattern @state) (rem step 0x40)) 0x40)))
(comment 
  (remove-step-fn 1 :play-break))

;; TODO Figure out why clock 2 is skipping all over the place
(defn clock2-step [step]
  )

(volume 1.0)

(on-event "/clock-tick"
          ;; #(println "/clock-tick" %)
          (fn [{:keys[args path]}]
            ;; (println args)
            (let [[num clock-id step] args
                  ]
              (cond
                (= clock-id 1) (clock1-step step)
                (= clock-id 2) (clock2-step step)
                )
              )
            )
          ::clock-tick)
(comment
  (remove-event-handler ::clock-tick)

  (recording-start "~/audio/soundfiles/jams/20210213a__20210222-0709.wav")
  (recording-stop)
  )


(swap! state merge 
            {
             :groove1a-amt 1.0
             :groove2a-amt 0.25
             :groove1a-fn m/pow2
             :groove2a-fn m/pow3
             :patterns []
             :root-pattern []
             :current-pattern []
             :pattern-gen-fn (fn [pat] pat)
             }))

;; IDEA: store all reducers in the state itself, perhaps use map keys of maps to
;; act as slices so we can reduce the conditionals that run each update.
(add-watch state :state-change
           (fn [key atom old-state new-state]
             (if-not (and (= (:root-pattern old-state) (:root-pattern new-state))
                          (= (:pattern-gen-fn old-state) (:pattern-gen-fn new-state)))
               (swap! state assoc :current-pattern ((:pattern-gen-fn new-state) (:root-pattern new-state))))))

(comment
  (remove-watch state :state-change))

(def l (taker 16))

(swap! state assoc :root-pattern
       (hexstrpat->vec "10 11 12 13 20 25 36 37 14 19 3A 3 2C 1D E 3F"))
(swap! state assoc :root-pattern
       (hexstrpat->vec "14 11 12 13 24 25 36 37 14 19 3A 3 2C 1D E 3F"))
(swap! state assoc :root-pattern
       (range 0 0x40))
(swap! state assoc :root-pattern
       (hexstrpat->vec
        "10 . . . . . . . 24 29 . . . . 6 17"))
(swap! state assoc :pattern-gen-fn
       (fn [pat] (let [pat (map (fn [step index] (if (< (rem index 16) 16) step ".")) pat (range))
                       pat (partition-map 8 (fn [subpat] (map #(if (< %2 2) %1 ".") subpat (range))) pat)
                       patv (vec pat)]
                   (concat pat pat pat pat 
                           ;; (l (cycle-sub pat 0 6 ))
                           ;; (l (cycle-sub pat 4 6 ))
                           ;; (l (cycle-sub pat 2 6 ))
                           )))
       )

(swap! state assoc :pattern-gen-fn
       (fn [pat] (let [pat (map (fn [step index] (if (< (rem index 16) 16) step ".")) pat (range))
                       patv (vec pat)](concat patv
                                              (l (cycle (subvec patv 0 6 )))
                                              (l (partition-map 8 reverse patv))
                                              (l (partition-map 6 reverse patv))
                                              )))
       )
(swap! state assoc :root-pattern (reverse (:root-pattern @state)))

(swap! state assoc :delay-set-fns [(fn []
                                     (ctl delay-1-tap-1 :index-offset (beats->samps 1.25 142))
                                     (ctl delay-1-tap-2 :index-offset (beats->samps 0.5 142)))
                                   (fn []
                                     (ctl delay-1-tap-1 :index-offset (beats->samps 0.125 142))
                                     (ctl delay-1-tap-2 :index-offset (beats->samps 1.75 142)))
                                   (fn []
                                     (ctl delay-1-tap-1 :index-offset (beats->samps 0.375 142))
                                     (ctl delay-1-tap-2 :index-offset (beats->samps 1.25 142)))
                                   (fn []
                                     (ctl delay-1-tap-1 :index-offset (beats->samps 0.25 142))
                                     (ctl delay-1-tap-2 :index-offset (beats->samps 1.50 142)))])

(comment 
  (definst mic [amp 1]
    (let [src (in (num-output-buses:ir))]
      (* amp src)))

  (def mic1 (mic))

  (pp-node-tree)
  (kill mic1)

  (stop)))

(on-event [:midi :note-on]
          (fn [m]
            (let [note (:note m)]
              (play-sample-step break (- note 20) 0x10)
              )
            )
          ::test-sensel-midi)

(comment 
  (remove-event-handler ::test-sensel-midi))

(defn quantize-val
  ([val steps]
   (quantize-val val steps 1)
   )
  ([val steps unit]
   (* (/ (quot val (/ unit steps)) steps)))
  )

(on-event [:midi :control-change]
          (fn [m]
            (let [control (:data1 m)
                  value (:data2-f m)
                  ]
              (run! (fn [[key func]] (func control m)) (:midi-cc-fns @state))
              ))
          ::test-sensel-midi-cc)

(defn handle-cc--delays [control m]
  (let [value (:data2-f m)
        quantized-value (quantize-val value 16)
        ]
    (if (= control 9)
      (ctl delay-1-tap-1 :out-amp value)
      )
    (if (= control 10)
      (ctl delay-1-tap-1 :feedback-amp value)
      )
    (if (= control 17)
      (ctl delay-1-tap-1 :index-offset (* quantized-value (beats->samps 2.0 142)))
      )
    (if (= control 13)
      (ctl delay-1-tap-2 :out-amp value)
      )
    (if (= control 14)
      (ctl delay-1-tap-2 :feedback-amp value)
      )
    (if (= control 18)
      (ctl delay-1-tap-2 :index-offset (* quantized-value (beats->samps 2.0 142))))
    )
  )

(swap! state assoc-in [:midi-cc-fns :delay-ccs] handle-cc--delays)
(swap! state assoc-in [:midi-cc-fns :debug] #(println (quantize-val (:data2-f %2) 16)))
(println "---------")
(comment 
  (remove-event-handler ::test-sensel-midi-cc))

(/ (quot 0.125 (/ 1 16)) 16)

(map {:a 1 :b 2} '(:a :b))
