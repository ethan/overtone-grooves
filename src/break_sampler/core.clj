(ns break-sampler.core
  (:use
   [overtone.core]
   )
  )

;; TODO: re-implement using a smarter sampler with proper envelopes
;; TODO implement rate-scaling to change speed
;; TODO implement granular and pitch shifting versions of rate scaling

(defn play-sample-from-hex [buf posHex posMaxHex]
  (if (not (= posHex "."))
    (let [posInt (Integer/parseInt posHex 16)
          maxInt (Integer/parseInt posMaxHex 16)
          pos (/ posInt maxInt)
          ]
      (mono-partial-player buf 1 pos (+ pos (/ 1 maxInt)))
      ))
  )

(defn play-sample-step [buf step num-steps]
  (if (not (= step "."))
    (let [pos (/ step num-steps)]
      (stereo-partial-player buf 1 pos (+ pos (/ 1 num-steps)))))
  )

