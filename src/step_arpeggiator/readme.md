A flexible arpegiator that stores one (or more?) queues of notes and determines
a note value for successive step triggers.

There are two key considerations in determining an arpeggiator pattern: the
function used to sort the queue, and the function used to select items from the
queue for any given step.

The most common arpeggiators use either note number or order of note on to sort
the queue, and ramp or triangle LFOs to determine the current note.

Others should also be possible, however. We might sort using less predictable
algorithms, and might calculate the next step by skipping every x in the
sequence. Stochaistic approaches could also be possible.

We might also add hooks to the enqueing functions to generate additional notes
based on the one pushed. E.g. push certain chords in full for each note added.
Or add octaves.

This implies the following flow:

```
enqueue -> pre-transform -> add -> sort -> select -> post-transform -> out
```

Sidenote: what would a Should provide a way to use MIDI on/off notes to
enqueue/dequeue.

Might also make an alternate implementation that is server-side.
