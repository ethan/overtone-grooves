(ns trainwrecks.1
  (:use
   [overtone.core]
   )
  (:require
   [fastmath.core :as m]
   [fastmath.interpolation :as m-i]
   [fastmath.easings :as m-e]
   [score.core :as s]
   )
  )

(connect-external-server)
(use 'overtone.inst.drum)
(use 'overtone.inst.synth)
(use 'groove-step-seq.groove-buffer)
(groove-buffer-synth-init)

;; TODO how to be more flexible re BPM?
(def groove1 (groove-metro-synth server-clock-b 104 4 4 1))
(def groove1-buffer-a (generate-groove-buffer
                       (make-interpolation-fn m/pow2) 104 16))
(write-groove-buffer groove1-buffer-a
                     (make-interpolation-fn m/pow2 :interpolation-amount 0.1)
                     104)

;; FIXME add a bunch of standard functions to groove_buffer namespace that are
;; initialized when init func is called

(def groove1-grooverA (add-groove-transform-synth groove1 groove-clock-shaper-stepped groove1-buffer-a ))

(comment
  (kill groove1-grooverA)
  )
(def groove2 (groove-metro-synth server-clock-b 104 4 4 2))
(def groove2-buffer-a (generate-groove-buffer
                       (make-interpolation-fn m/pow2) 104 16))
(write-groove-buffer groove2-buffer-a
                     (make-interpolation-fn m/pow2 :interpolation-amount 0.1)
                     104)
(def groove2-grooverA (add-groove-transform-synth groove2 groove-clock-shaper-stepped groove2-buffer-a))

;; TODO build these as "components" that have both synth controls and state watchers
(def groove2-buffer-b (generate-groove-buffer
                       (make-interpolation-fn m/pow3 :interpolation-amount 0.25)
                       104 4))
;; (write-groove-buffer groove2-buffer-b groove2b-fn 104)
(def groove2-grooverB (add-groove-transform-synth groove2 groove-clock-shaper-stepped groove2-buffer-b))
(comment
  (kill groove2-grooverB)
  )

(def state (atom {:groove1a-amt 1.0 :groove2a-amt 0.25 :groove1a-fn m/pow2 :groove2a-fn m/pow3}))
(swap! state assoc :groove1a-amt 0.02)
(swap! state assoc :groove2a-amt 0.02)
(swap! state assoc :groove2b-amt 0.03)
(swap! state assoc :groove1a-fn #(m/safe-sqrt %))
(swap! state assoc :groove2a-fn #(m/pow2 %))
(swap! state assoc :groove2a-fn m/pow2)
(swap! state assoc :kick-pattern #{0 4 6 8 12 14})
(swap! state assoc :bing-pattern #{0 2 4 6 8 10 12 14})

(defn reduce-pattern-sets [& patterns]
  "Combine a set of patterns defines as trigger step numbers"
  (set (reduce concat
               (map
                (fn [start set]
                  (map #(+ (* 16 start) %) set)
                  )
                (range) patterns)
               )))

(swap! state assoc :kick-pattern
       (reduce-pattern-sets
        [0 1 2 3 4 6 8 ]
        [6 10]
        [10]
        [0 10 14]
        ))

(add-watch state :state-change
           (fn [key atom old-state new-state]
             (if-not (= (:groove1a-amt old-state) (:groove1a-amt new-state))
               (ctl groove1-grooverA :amount (:groove1a-amt new-state))
               )
             (if-not (= (:groove2a-amt old-state) (:groove2a-amt new-state))
               (ctl groove2-grooverA :amount (:groove2a-amt new-state))
               ;; (write-groove-buffer groove2-buffer-a (make-interpolation-fn m/pow3 :interpolation-amount (:groove2a-amt new-state)) 104)
               )
             (if-not (= (:groove2b-amt old-state) (:groove2b-amt new-state))
               (ctl groove2-grooverB :amount (:groove2b-amt new-state))
             ;;   (write-groove-buffer groove2-buffer-b (make-interpolation-fn m/pow3 :interpolation-amount (:groove2b-amt new-state)) 104)
               )
             (if-not (= (:groove1a-fn old-state) (:groove1a-fn new-state))
               (write-groove-buffer groove1-buffer-a (make-interpolation-fn (:groove1a-fn new-state) :interpolation-amount (:groove1a-amt new-state)) 104)
               )
             (if-not (= (:groove2a-fn old-state) (:groove2a-fn new-state))
               (write-groove-buffer groove2-buffer-a (make-interpolation-fn (:groove2a-fn new-state) :interpolation-amount (:groove2a-amt new-state)) 104)
               )
             )
           )

(comment
  (remove-watch state :state-change)
  )

(def sample-test (sample "~/audio/samples/xi/CATEGORY/PERCUSS/DRUMMACH/ROLAND/TR909A/909bd3.xi"))

(def snare-sample-test (sample "~/audio/samples/xi/CATEGORY/PERCUSS/DRUMMACH/ROLAND/TR606A/snarea.xi"))

(def snare-buffer (load-sample "~/audio/samples/xi/CATEGORY/PERCUSS/DRUMMACH/ROLAND/TR606A/snarea.xi"))
(def kick-buffer (load-sample "~/audio/samples/xi/CATEGORY/PERCUSS/DRUMMACH/ROLAND/TR606A/bdruma.xi"))
(definst play-kick-sample [sample-buffer 0 dur 0]
  (let [
        dur (if (= dur 0)
              (buf-dur:kr sample-buffer)
              dur
              )
        ;; TODO add buffer rate
        index (line:ar 0 (* (buf-sample-rate:kr sample-buffer) dur) dur :action FREE)
        ]
    (buf-rd:ar 1 sample-buffer index)
    )
  )

(definst play-snare-sample [sample-buffer 0 dur 0]
  (let [
        dur (if (= dur 0)
              (buf-dur:kr sample-buffer)
              dur
              )
        ;; TODO add buffer rate
        index (line:ar 0 (* (buf-sample-rate:kr sample-buffer) dur) dur :action FREE)
        ]
    (buf-rd:ar 1 sample-buffer index)
    )
  )

(inst-fx! play-snare-sample fx-freeverb)
(inst-fx! play-snare-sample fx-noise-gate)
(inst-fx! play-kick-sample fx-freeverb)
(inst-fx! overpad fx-reverb)
(play-sample-envd snare-buffer)
(stop)

(defn clock1-step [step]
  (if (contains? (:bing-pattern @state) (int (rem step 16))) (bing))
  (if (contains? (:kick-pattern @state) (int (rem step 64)))
    ;; (dance-kick)
    (play-kick-sample kick-buffer)
    )
  (if (= 0.0 (rem step 2)) (closed-hat2))
  (if (= 4.0 (rem step 8))
    ;; (noise-snare)
    (play-snare-sample snare-buffer)
    )
  ;; (if (= 0.0 (rem step 16)) (mapv #(overpad % :amp 0.25) [68 56 65]))
  )

;; TODO Figure out why clock 2 is skipping all over the place
(defn clock2-step [step]
  (if (contains? (:bing-pattern @state) (int (rem step 16))) (bing))
  ;; (if (= 4.0 (rem step 8)) (mapv #(overpad % :amp 0.25) [49 56 61]))
  ;; (if (contains? (:kick-pattern @state) (int (rem step 64))) (dance-kick))
  ;; (if (= 0.0 (rem step 1)) (closed-hat2 :amp (/ 0.15 (+ 1 (rem step 3)))))
  ;; (if (= 4.0 (rem step 8)) (noise-snare))
  ;; (case (rem step 4)
  ;;   2.0 (bass (midi->hz 37))
  ;;   (bass (midi->hz 25))
  ;;   )
  )

(volume 0.5)

(on-event "/clock-tick"
          ;; #(println "/clock-tick" %)
          (fn [{:keys[args path]}]
            ;; (println args)
            (let [[num clock-id step] args
                  ]
              (cond
                (= clock-id 1) (clock1-step step)
                (= clock-id 2) (clock2-step step)
                )
              )
            )
          ::clock-tick)
(remove-event-handler ::clock-tick)

(recording-start "~/audio/recordings/2021-01-31--swing-test-first-state.wav")
(recording-stop)

;; IDEA: two time scales: one for trigger and one to look up the current step
;; e.g. log lookup with linear swung trigger could yield lots of repeats toward
;; the end at a pretty even clip

(comment 
  (ctl groove1-grooverA :amount 0.0)
  ;; FIXME why aren't these working?
  (ctl groove1-grooverA :mode 0)
  (ctl groove1-grooverA :period 100)
  (ctl groove1-grooverA :period 0)
  )

;; TODO figure out why this is skipping at the end patterns
