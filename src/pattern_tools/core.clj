(ns pattern-tools.core
  (:require
   [clojure.string :as str]
   ))

(defn taker
  "Creates partials to make it easy to take x from a lazy seq.
  Common usage `(def t16 (taker 16))`"
  [len]
  (partial take len)
  )

(defn hexstrpat->vec
  "Convert a string of space separated hexes to a vector.
  E.g. `'0 F 4` => `[0 15 4]`"
  [string]
  (mapv
   #(if (not (= % ".")) (Integer/parseInt % 16) %)
   (str/split string #" ")))


(defn partition-map
  "Map a fn over regular partitions of a seq.
  e.g. `(partition-map 2 reverse '(1 2 3 4))` yields `(2 1 4 3)`"
  [n op-fn obj]
  (mapcat op-fn 
          (partition n (cycle obj)))
  )

(defn map-sub-walk
  "Apply a fn to partitions of a given length starting from a given index that incremenet by some amount."
  ([l func partition-length]
   (map-sub-walk l func 0 partition-length))
  ([l func start partition-length]
   (map-sub-walk l func start partition-length partition-length))
  ([l func start partition-length increment]
   (reduce (fn [l-acc idx]
             (let [sub-l (take partition-length (drop idx l-acc))
                   map-sub-l (func sub-l)
                   ]
               (concat (take idx l-acc) map-sub-l (drop (+ idx partition-length) l-acc)))
             )
           l
           (range start (count l) increment)
           )))

(defn cycle-sub [l start len]
  (cycle (take len (drop start l)))
  )

(defn cycle-subvec [l start len]
  (cycle (subvec l start len))
  )

;; Add braid

;; TODO add filter to drop every X, or drop according to a pattern

;; TODO use - nums for different sorts of skips instead of "."

;; TODO add rotation and swap pattern mutation funcs

;; FIXME Old functions to parse pattern strings

(comment
  (defn str-nth [string n]
    (nth (str/split string #" ") n))

  (defn play-hex-pattern-step [index pattern fn]
    (fn (str-nth pattern index)))

  (play-hex-pattern-step 3 pattern-string #(playSampleFromHex break % "40")))
