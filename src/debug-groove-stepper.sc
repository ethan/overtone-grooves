s = Server(\remoteServer, NetAddr("127.0.0.1", 57110 ))
s = Server.remote(\remoteServer,NetAddr("127.0.0.1",57110))

w = Window.new.front;
w.onClose = { // free everything when finished
    c.stop;
    "SynthDefs, busses and buffers have all been freed.".postln;
};

c = ScopeView.new(w.view, w.view.bounds.insetAll(10,10,10,10));
c.bufnum = 4;
c.server = s;

c.style = 0; // vertically spaced
c.style = 1; // overlapped
c.style = 2; // x/y

s.cachedBufferAt(4)
s.serverRunning

c.start
c.stop

	b = Buffer.new(s,9000,1,4);
	b.plot

o = Stethoscope.new(s, numChannels: 2, index: 0, bufsize: 4096, zoom: 1.0, rate: 'control', w.view, 4)

ScopeView
