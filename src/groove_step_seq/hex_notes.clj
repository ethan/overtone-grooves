;; Library to write scale-based tonal music in hex.
;; This is inspired by Steven Yi's hex rhythm work.
;;
;; The basic concept is to use the left to right bitwise representation of hex
;; codes to notate the on/off states of notes in a 7 note scale.
;;
;; For example:
;;
;; 01 = 1000 0000 = The tonic
;; 02 = 0100 0000 = Second
;; 04 = 0010 0000 = the Third
;; 05 = 1010 0000 = a third interval, from the tonic
;; 11 = 1000 1000 = a fifth
;; 14 = 1010 1000 = a triad
;;
;; The interface for creating vectors of notes is simple:
;;
;; (hex->notes pitch-hex [:octave hex :sharps hex flats: hex] )
;;
;; for example:
;;
;; (hex->notes 0x14 :oct 0xa :sharps 0x10) ;; Augmented 5th.
