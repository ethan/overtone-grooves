(ns groove-step-seq.groove-buffer
  (:use
   [overtone.core]
   [overtone.sc.machinery.synthdef :only [synthdef?]]
   )
  (:require
   [fastmath.core :as m]
   ))

;; TODO add start and end indexes so we can compose/piecewise funcs more easily


;; TODO ideally most of the factor args would be implemented dynamically in the
;; metro processing synths
(defn make-interpolation-fn [interpolation-fn
                             & {:keys [interpolation-amount rate-factor scale-factor interpolation-fn-loop-period add mult loop?]
                                :or {interpolation-amount 1.0
                                     ;; "speed" modifier
                                     rate-factor 1.0
                                     ;; calc period ratio
                                     scale-factor 1.0
                                     ;; loop over fraction
                                     interpolation-fn-loop-period 1.0
                                     add 0.0
                                     mult 1.0
                                     ;; Should we loop around period of ticks,
                                     ;; or step up?
                                     loop? false 
                                     }}]
  ;; TODO should interpolation amount be generator or fn arg?
  ;; len is an arg to the fn because it may vary with buffer length, which we
  ;; don't know when we create the fn.
  (fn [t len]
    (let [
          ;; TODO Figure out how to shift window around current time without skipping
          ;; len-limit (* len (/ 1 1))
          ;; offset (* len (/ 0 1))
          ;; rate-scaled-t (+ offset (mod (* t rate-factor) len-limit))
          rate-scaled-t (* t rate-factor)
          rate-scaled-t (if loop? (mod rate-scaled-t len) rate-scaled-t)
          ;; convert scaled to to 1..scale-factor over length of buffer
          ;; (allows us to more rapidly or slowly run through the output of the func)
          buffer-phase (* rate-scaled-t (/ scale-factor len))
          phase-start (* len (quot rate-scaled-t len))
          ]
      (-> buffer-phase
          ;; Set a period for the interpolation fn looops if we've chosen to
          (mod interpolation-fn-loop-period)
          (* (/ 1 interpolation-fn-loop-period))
          interpolation-fn
          (+ add)
          (* mult)
          ;; TODO determine if this is actually needed, and, if so, where the
          ;; best place for it is. It doesn't work here because it messes up
          ;; things like trig functions that need to be above 1.0.
          ;; ((partial m/wrap 0.0 1.0))
          (/ scale-factor)
          (* interpolation-fn-loop-period)
          (+ (* interpolation-fn-loop-period (m/floor (/ buffer-phase interpolation-fn-loop-period))))
          ;; Normalize to length of buffer
          (* len)
          ;; Average between linear transformed amount (amt of warp)
          (#(+ phase-start (* % interpolation-amount) (* rate-scaled-t (- 1 interpolation-amount))))
          )
      )
    )
  )

(defn make-trig-interpolation-fn [trig-interpolation-fn & rest]
  (apply make-interpolation-fn (fn [t]
                                 (-> t
                                     ;; Container for trig functions:
                                     ;; Scale 0..1 to 0..2PI
                                     (* m/PI 2)
                                     trig-interpolation-fn
                                     )
                                 ) rest)
  )


(defn write-groove-buffer [groove-buffer groove-buffer-fn bpm]
  (let [len (num-frames groove-buffer)
        groove-vector (mapv (fn [n] (groove-buffer-fn n len)) (range 0 len))
        filled-groove-buffer (buffer-write-relay! groove-buffer groove-vector)]
    filled-groove-buffer))

(defn buffer-length-from-bpm-beats [bpm num-beats]
  (let [bps (/ bpm 60)
        spb (/ 1 bps)
        ticks-per-second (server-control-rate)
        length-in-seconds (* num-beats spb)]
    ;; Based on
    ;; https://github.com/overtone/overtone/blob/master/src/overtone/sc/clock.clj
    ;; it looks like it's likely microseconds 
    (* ticks-per-second length-in-seconds)))

(defn generate-groove-buffer [groove-buffer-fn bpm beats]
  (-> (buffer-length-from-bpm-beats bpm beats)
      (buffer)
      (write-groove-buffer groove-buffer-fn bpm)))

(defn groove-buffer-synth-init []
  [(defsynth clock-in [small-tick-in-bus 0 clock-tick-out-bus 0]
     (replace-out:kr clock-tick-out-bus (in:kr small-tick-in-bus 1)))

   ;; TODO figure out how to include this in ns prior to booting server
   ;; Maybe wrap in an init func?


   (defsynth clock-tick-stepper [clock-bus 0 clock-tick-bus 1 bpm 60 tpb 4]
     (let [
           ;; TODO figure out how to best handle 1 vs 2 channel clocks
           ;; E.g. for Overtone internal clock, should get small tick and big tick from the overtone clock
           ;; [s-t b-t] (in:kr clock-bus 2)
           ;; This implementation will hiccup when small-tick wraps around and
           ;; big-tick increments.
           in-clock (in:kr clock-bus 1)
           ;; FIXME can we assume seconds per tick is always server control dur?
           ;; TODO support variable tick freq
           seconds-per-tick (server-control-dur)
           clock-seconds (* in-clock seconds-per-tick)
           bps (/ bpm 60)
           spb (/ 1 bps)
           clock-beat-count (/ clock-seconds spb)
           ;; Int ticks (beat subdivisions)
           clock-tick-num (floor (* tpb clock-beat-count))
           ;; Fractional beats
           ;; TODO determine if we should send beats on a separate channel
           ;; clock-beat-num (floor clock-beat-count)
           ;; clock-beat-subdiv (/ clock-tick-num tpb)
           ]
       ;; (send-trig:kr tick-changed-trig tick-num)
       (replace-out:kr clock-tick-bus clock-tick-num)))

   (defsynth tick-metro-trigger [metro-in-bus 0 message-id 1]
     (let [tick-num (in:kr metro-in-bus 1)
           tick-changed-trig (absdif tick-num (delay1:kr tick-num))]
       ;; TODO use macros or similar to allow changing of message name as well.
       (send-reply:kr tick-changed-trig "/clock-tick" [tick-num] message-id)))

   (defsynth clock-bar-stepper [clock-bus 0 clock-bar-bus 1 bpm 60 bpb 4]
     (let [in-clock (in:kr clock-bus 1)
           seconds-per-tick (server-control-dur)
           clock-seconds (* in-clock seconds-per-tick)
           bps (/ bpm 60)
           seconds-per-beat (/ 1 bps)
           seconds-per-bar (* seconds-per-beat bpb)
           clock-bar-count (/ clock-seconds seconds-per-bar)
           clock-bar-num (floor clock-bar-count)]
       (replace-out:kr clock-bar-bus clock-bar-num)))

   (defsynth bar-metro-trigger [metro-in-bus 0 message-id 1]
     (let [tick-num (in:kr metro-in-bus 1)
           tick-changed-trig (absdif tick-num (delay1:kr tick-num))]
       ;; TODO use macros or similar to allow changing of message name as well.
       (send-reply:kr tick-changed-trig "/clock-bar" [tick-num] message-id)))

   (defsynth groove-clock-shaper-stepped
     "Transforms an input clock signal by a lookup table. Takes an optional period in ticks to compute the lookup rate, period defaults to buffer len. Defaults to stepping to linear value every new period, but can optionnaly loop or accumulate based on final Y of last value in lookup table * num periods (mode 0, 1, or 2)."
     [clock-bus 0 groove-clock-bus 1 groove-buffer 1 amount 1.0 period 0]
     (let [
           ;; First get small tick from the overtone clock
           ;; TODO support big tick / bar ticks as well
           in-clock (in:kr clock-bus 1)
           groove-buffer-frames (buf-frames:kr groove-buffer)
           lookup-rate (if (= period 0)
                         1
                         (/ groove-buffer-frames period)
                         )
           ;; Then pass that through the groove buffer transform
           ;; (returns num ticks mapped, not seconds)
           in-clock (* in-clock lookup-rate)
           period-start-value (* groove-buffer-frames (floor (/  in-clock groove-buffer-frames)))
           ;; period-start-value 0
           groove-buffer-index (+ period-start-value (mod in-clock groove-buffer-frames))
           ]
       ;; Read the corresponding buffer value ("waveshaping" the clock)
       (->> (buf-rd:kr 1 groove-buffer groove-buffer-index 1)
           (#(+ (* (- 1 amount) groove-buffer-index) (* amount %)))
           (+ period-start-value) 
           (* (/ 1 lookup-rate))
           (replace-out:kr groove-clock-bus)
           )
       ))

   (defsynth groove-clock-shaper
     "Transforms an input clock signal by a lookup table. Always assumes 1:1 ratio of clock signal to buffer rate, and always loops."
     [clock-bus 0 groove-clock-bus 1 groove-buffer 1]
     (let [
           ;; First get small tick from the overtone clock
           ;; TODO support big tick / bar ticks as well
           in-clock (in:kr clock-bus 1)
           ;; Then pass that through the groove buffer transform
           ;; (returns num ticks mapped, not seconds)
           groove-buffer-frames (buf-frames:kr groove-buffer)
           groove-buffer-index (mod in-clock groove-buffer-frames)
           ;; Read the corresponding buffer value ("waveshaping" the clock)
           groove-result (buf-rd:kr 1 groove-buffer groove-buffer-index 1)
           ;; Now compute the number of seconds that result coresponds to
           ]
       ;; (send-trig:kr tick-changed-trig tick-num)
       ;; (send-reply:kr groove-tick-changed-trig message-key [groove-tick-num groove-beat-subdiv s-t groove-result groove-buffer-index groove-buffer-frames] 1)
       (replace-out:kr groove-clock-bus groove-result)))

   (defsynth groove-clock-offsetter [clock-bus 0 groove-clock-bus 1 groove-buffer 1 mult 1]
     (let [
           ;; First get small tick and big tick from the overtone clock
           in-clock (in:kr clock-bus 1)
           ;; Then pass that through the groove buffer transform
           ;; (returns num ticks mapped, not seconds)
           groove-buffer-frames (buf-frames:kr groove-buffer)
           ;; TODO may want to make this an arg so we can change the lookup size on
           ;; the fly as tempo changes, etc.
           groove-buffer-index (mod in-clock groove-buffer-frames)
           ;; Read the corresponding buffer value ("waveshaping" the clock)
           groove-buffer-value (buf-rd:kr 1 groove-buffer groove-buffer-index 1)
           ;; Add the offset table value to the clock value, with a multiplier to
           ;; adjust amount of offset.
           groove-result (+ in-clock (* mult groove-buffer-value))]
       ;; (send-trig:kr tick-changed-trig tick-num)
       ;; (send-reply:kr groove-tick-changed-trig message-key [groove-tick-num groove-beat-subdiv s-t groove-result groove-buffer-index groove-buffer-frames] 1)
       (replace-out:kr groove-clock-bus groove-result)))])

(defn ensure-groove-buffer-synths []
  (if-not (every? identity (mapv #(= overtone.sc.synth.Synth (type %)) [clock-in tick-metro-trigger bar-metro-trigger clock-tick-stepper clock-bar-stepper]))
    (groove-buffer-synth-init)
    ))



(defn groove-interpolator [interpolator-fn xs-in-beats ys-in-amount bpm ys-amount-beat-ratio]
  (let [bps (/ bpm 60)
        spb (/ 1 bps)
        ticks-per-second (server-control-rate)
        ticks-per-beat (* ticks-per-second spb)
        xs-in-samples (mapv #(* % ticks-per-beat) xs-in-beats)
        y-amount (* ticks-per-beat ys-amount-beat-ratio)
        ys-in-samples (mapv #(* % y-amount) ys-in-amount)
        interpolator (interpolator-fn xs-in-samples ys-in-samples)]
    interpolator))

(defrecord GrooveMetroSynth [small-tick-in-bus bpm tpb bpb trigger-id groove-clock-group groove-clock-bus groove-clock-tick-bus groove-clock-bar-bus groove-clock-in groove-clock-tick-stepper groove-clock-bar-stepper groove-clock-tick-trigger groove-clock-bar-trigger groove-transform-synths])

;; TODO add ensure-groove-buffer-synth-init func
;; TODO create our own group instead of using the foundation timing group


(defn groove-metro-synth [small-tick-in-bus bpm tpb bpb trigger-id]
  (ensure-groove-buffer-synths)
  (let [groove-clock-group (group "Groove Clock Group" :after (foundation-timing-group))
        groove-clock-bus (control-bus 1)
        groove-clock-tick-bus (control-bus 1)
        groove-clock-bar-bus (control-bus 1)
        groove-clock-in (clock-in [:head groove-clock-group] small-tick-in-bus groove-clock-bus)
        groove-tick-stepper (clock-tick-stepper [:tail groove-clock-group] groove-clock-bus groove-clock-tick-bus bpm tpb)
        groove-tick-trigger (tick-metro-trigger [:after groove-tick-stepper] groove-clock-tick-bus trigger-id)
        groove-bar-stepper (clock-bar-stepper [:tail groove-clock-group] groove-clock-bus groove-clock-bar-bus bpm tpb)
        groove-bar-trigger (bar-metro-trigger [:after groove-bar-stepper] groove-clock-bar-bus trigger-id)
        groove-transform-synths []]
    (GrooveMetroSynth. small-tick-in-bus bpm tpb bpb trigger-id groove-clock-group groove-clock-bus groove-clock-tick-bus groove-clock-bar-bus groove-clock-in groove-tick-stepper groove-bar-stepper groove-tick-trigger groove-bar-trigger groove-transform-synths)))

(defn add-groove-transform-synth [groove-metro-synth groove-transform-synth & args]
  (let [metro-group (:groove-clock-in groove-metro-synth)
        clock-bus (:groove-clock-bus groove-metro-synth)
        groove-transform-synth-instance (apply (partial groove-transform-synth [:after metro-group] clock-bus clock-bus) args)
        ]
    groove-transform-synth-instance
    ))

(defn kill-groove-synth [groove-metro-synth]
  (kill (:groove-clock-group groove-metro-synth))
  )

(comment
  (connect-external-server)
  ;; TODO extract this into a regular interface
  ;; TODO implement piecewise functions and compose smaller subdivisions for
  ;; things like swing
  (defn groove-buffer-fn [t len]
    (-> t
        (* 1.0)
        ;; (+ (* 0.75 1000 (m/sin (* m/PI 2 t (/ 4 9000)))))
        ;; (* (m/sq (/ t 9000)))
        ;; (#(* % (m/smooth-interpolation 0.0 1.0 (/ % 9000))))
        ;; Not sure why we can't just go from 0.0 to len
        ;; (#(* len (m/quad-interpolation 0.0 1.0 (/ % len))))
        ;; Average between linear the last version
        ;; (#(+ (/ % 2) (/ t 2)))
        (#(* len (m/safe-sqrt (/ % len))))))

  (def groove-buffer-1 (generate-groove-buffer groove-buffer-fn 80 16))
  groove-buffer-1
  (write-groove-buffer groove-buffer-1 groove-buffer-fn 80)
  (buffer-info groove-buffer-1)

  (def groove-clock-bus-1 (control-bus 1 "Groove Clock Bus 1"))

  (def groove-shaper (groove-clock-shaper [:tail (foundation-timing-group)] server-clock-b groove-clock-bus-1 groove-buffer-1))
  (def groove-stepper (clock-tick-stepper [:tail (foundation-timing-group)] groove-clock-bus-1 groove-clock-bus-1 80 4))
  (def groove-tick-trigger (tick-metro-trigger [:tail (foundation-timing-group)] groove-clock-bus-1 1))

  ;; Cleanup code:
  (kill groove-shaper)
  (kill groove-stepper)
  (kill groove-tick-trigger)

  (def clock-clock-bus-1 (control-bus 1 "Clock Clock Bus 1"))
  (def clock-stepper (clock-tick-stepper [:tail (foundation-timing-group)] server-clock-b clock-clock-bus-1 80 4))
  (def clock-tick-trigger (tick-metro-trigger [:tail (foundation-timing-group)] clock-clock-bus-1 2))
  (kill clock-stepper)
  (kill clock-tick-trigger)

  (defn clock-step [step]

    (let [mod-step (mod step 16)
          hat-fn (hat-step mod-step)
          kick-fn (kick-step mod-step)
          snare-fn (snare-step mod-step)]
      ;; (at (now)
      ;; (println args)
      (hat-fn)
      ;; (kick-fn)
      (snare-fn)
      (cond
        (= (mod step 4) 2.0) (bass :freq (midi->hz 39) :amp 0.5))
      ;; )
      ))
  (on-event "/clock-tick"
            #(println "/clock-tick" %)
            ;; TODO figure out how to destructure the packet that comes in with the
            ;; reply
            ;; (fn [{:keys[args path]}]
            ;;   (let [[id num step] args
            ;;         ]
            ;;     (clock-step step)
            ;;     )
            ;;   )
            ::clock-tick)

  (remove-event-handler ::clock-tick)

  (use 'overtone.inst.drum)
  (dance-kick))
