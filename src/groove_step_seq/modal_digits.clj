(ns groove-step-seq.modal-digits
  ;; (:use
  ;;  [overtone.core]
  ;;  )
  )


(def second (* 3/2 3/2 1/2))
(def major-mode-ratios [1 second 5/4 4/3 3/2 5/3 12/7])

(defn get-root-hz-from-reference-note
  "Get the hz for the root note based on a reference octave-degree/hz pair."
  [mode radix reference-hz reference-octave-degree]
  (let [reference-degree (mod reference-octave-degree radix)
        reference-octave (/ (- reference-octave-degree reference-degree) radix)
        reference-degree-ratio (mode reference-degree)
        reference-scale-ratio (Math/pow 2 reference-octave)
        ;; Note that root is defined as the 0 note, note the center note.
        root-ratio (/ 1 (* reference-degree-ratio reference-scale-ratio))
        ]
    (* reference-hz root-ratio)
    )
  )

(defn int->hz
  "Convert an int representing a note's octave and scale degree into a hertz
  value based on a given mode's ratios."
  ;; TODO radix may be redudant, since should always = count of mode
  ([note-radix-int & {:keys [radix mode-ratios reference-hz reference-radix-int]
                      :or {radix 7
                           mode-ratios major-mode-ratios
                           reference-hz 440
                           reference-radix-int 7r030
                           }
                      }]
   (let [root-hz (if (= reference-radix-int 0)
                   reference-hz
                   ;; TODO with this many vars passed, is this truly a separate function?
                   (get-root-hz-from-reference-note mode-ratios radix reference-hz reference-radix-int))
         note-degree (mod note-radix-int radix)
         note-octave (/ (- note-radix-int note-degree) radix)
         degree-ratio (mode-ratios note-degree)
         octave-ratio (Math/pow 2 note-octave)
         ]
     (* root-hz degree-ratio octave-ratio)
     )
   )
  )

(defn splitv-intr
  "Split an int every n digits using a given radix."
  [int-in radix digits]
  (let [divisor (Math/pow radix digits)
        remainder (int (mod int-in divisor))
        result-int (int (/ (- int-in remainder) divisor))
        ]
    (if (> result-int 0)
      (conj (splitv-intr result-int radix digits) remainder)
      [remainder]
      )
    )
  )

(defn map-splitv-intr
  "Split each of a vector of ints, creating a 2d vector of note-ints."
  [vec-in radix digits]
  (mapv #(splitv-intr % radix digits) vec-in)
  )

(defn mtranspose-vec
  ([vec-in fill-value]
   ;; TODO this could be optimized by using a matrix library, if we want the
   ;; additional dep.
   (let [first-column (mapv #(if (not-empty %) (first %) fill-value) vec-in)
         remaining-columns (mapv (#(if (not-empty %) (subvec % 1) [])) vec-in)
         has-remaining? (some not-empty remaining-columns)
         ]
     (if has-remaining?
       (cons first-column (mtranspose-vec remaining-columns fill-value))
       [first-column]
       )
     )
   )
  ([vec-in]
   (mtranspose-vec vec-in 0)
   )
  )

(defn mtranspose-splitv-intr
  "Rotate a matrix of intr's to create vectors of the first, second, etc. int of
  a given digit length in each."
  [vec-in radix digits]
  (-> vec-in
      (map-splitv-intr radix digits)
      (mtranspose-vec)
      )
  )
