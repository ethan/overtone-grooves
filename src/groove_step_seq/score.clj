(ns groove-step-seq.score
  (:use
   [overtone.core]
   ;; [overtone.live]
   [groove-step-seq.groove-steps])
  (:require [overtone.ableton-link :as link]
            [score.core :as score]
            )
  )


(connect-external-server)

(definst baz [freq 440] (* 0.3 (sin-osc freq)))

(def base-freq (note :b4))
(baz base-freq)
(baz (fifth base-freq))
(baz (third (* 2 base-freq)))
(baz (sixth (* 2 base-freq)))
(baz (fourth (* 4 base-freq)))
(baz (octave (* 2 base-freq)))
(baz (fifth (* 8 base-freq)))
(baz (third (* 4 base-freq)))
(baz (* 8/15 (* 8 base-freq)))
(kill baz)

(recording-start "~/audio/sin-chord2.wav")
(recording-stop)
