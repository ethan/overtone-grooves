(ns groove-step-seq.groove-steps
  "Functions to tranform a matrix of beat timings according to standard
  expressive timing offset algorithms.

  All these functions operate on a 'groove grid', which is a vector of offsets,
  expressed as fractions of a beat deltas to be added to each step in a cycle.

  The intent is that they can easily be composed and a metro beat output can be
  fed through them to derive a groove quantized set of beats for step
  sequencing.

  The are derived from W. Bas de Haas (2007).

  To summarize, options for expressive timing are:

  - Displacement: a single beat is displaced without altering the timing of others.
  - Offset: a beat is displaced and following beats are displaced a
    corresponding amount.
  - Cyclic displacement: beats are displaced according to a standard formula
    that takes beat number as an input. E.g. every 4th beat is displaced by a
    certain amount.
  - Tempo change: a range of beat is recalculated according to a different tempo
    from the overall composition tempo (could involve the overall pattern length
    staying the same).

  In thinking about implementation a few other considerations have emerged:

  - How to best reset / catch up after a displacement or other expressive timing
    invocation? E.g. displace the 2 then increase tempo to catch up by the 4.
  - Could add a 'reset' function that would reset all beats in a range, this
    could help when composing groove timing functions.
  - I wonder if these could be layered on top of a metro, so you pass a metro
    through a bunch of transforms to get grooved timings.
  - How to most easily handle case where you don't want to end up back on a
    straight rhythm? e.g. slow tempo then end on a displaced 4?

  From these here are some functions to add to the above:

  - Offset and Interpolate: offset a beat by a given amount, then scale the
    remaining beats by progressively decreasing amount such that they are back on
    the quantized rhythm after a given interval / at a given beat. The
    interpolation function could be customizable (linear, log, etc.)
  - Reset: reset the grid at a certain beat. That beat and all following are reset to grid quantization (or other?)
  - Interpolate: adjust one grid to be more or less like another, 'morphing'
    from one groove to another.
  - Velocity: progressively increase tempo offset according to some velocity.
  - Boomerang (need more proper name): specify a springback point, prior to that
    point tempo/velocity is one offset, after that point it is a corresponding
    inverse value such that at the end of the modification period the effective
    rate is the straight grid for the period's beat count.
  - Displace and Scale: displace one or more, then scale the following so that
    at end scale period we are back on straight rhythm.
  "
  ^{:doc "Functions to create groove quantization matrices."
    :author "Ethan Winn"}
  (:use [overtone.music time]
        [overtone.music rhythm]))

;; TODO figure out how to create a "groove-metro" that extends the metro API
;; with groove functions.

;; TODO figure out how to get the use in ns to work.
(use 'overtone.music.rhythm)

(defn vec-concat
  [v1 & rest]
  (reduce into v1 rest))

(defn vec-set
  "Return a copy of a vector with index set to value."
  [vec index value]
  (let [
        before (subvec vec 0 index)
        after (subvec vec (inc index))
        ]
    (vec-concat before [value] after)
    )
  )

(defn groove-init
  "Initialize a groove grid of length n"
  [length]
  (vec (replicate length 0))
  )

(defn groove-displace-beat
  "Displace a single beat in a grid vector by a given amount"
  [grid amt beat-index]
  (let [
        initial-val (nth grid beat-index)
        new-val (+ initial-val amt)
        ]
    (vec-set grid beat-index new-val)
    )
  )

;; TODO generalize this to apply any other transform cyclically.
(defn groove-displace-every
  "Displace every x beats by a given amount"
  [grid amt start-index period end-index]
  (if (< start-index end-index)
    (-> grid
        (groove-displace-beat ,,, amt start-index)
        (groove-displace-every ,,, amt (+ start-index period) period end-index))
    grid
    )
  )

(defn groove-offset
  "Offset a given beat and all following by a given amount."
  [grid amt start-index end-index]
  (groove-displace-every grid amt start-index 1 end-index)
  )

;; TODO how to account for the fact that beats are probably 1/16 notes, e.g.
;; steps not beats actually?
(defn groove-set-tempo
  "Set an alternate tempo for a group of beats."
  [grid tempo-multiplier start-index length]
  (let [end-index (+ start-index length)
        grid-slice (subvec grid start-index end-index)
        before (subvec grid 0 start-index)
        after (subvec grid end-index)
        beat-delta (- 1 tempo-multiplier)
        step-delta (/ beat-delta 4) ;; TODO support non-4/4 timing
        new-slice (mapv (fn [step-offset step-num] (+ step-offset (* step-num step-delta))) grid-slice (iterate inc 1))
        new-groove (vec-concat before new-slice after)
        ]
    new-groove
    )
  )

(defn groove-metro-beat
  "Get a beat from a metro filtered through a groove quantization grid."
  [grid m beat-index] ;; TODO do we need to put in -> compatible order, as here?
  (let [step-num (int (mod (* beat-index 4) 16)) ;; TODO support other time signatures
        beat-len (beat-ms 1 (metro-bpm m))
        offset-amt (nth grid step-num)
        offset-len (* offset-amt beat-len)
        ]
    (+ (m beat-index) offset-len)
    )
  )

(comment 
  (do
    (def base-grid (groove-init 16))
    (def tempo-grid (groove-set-tempo base-grid 0.75 0 8))
    (def displace-grid (groove-displace-beat base-grid -0.1 8))
    ;; TODO end-index feels off, allow default and also maybe use len or num
    ;; periods.
    (def cycle-grid (groove-displace-every base-grid 0.1 4 8 15))

    (def m (metronome 60))
    (-
     (groove-metro-beat base-grid m 1)
     (groove-metro-beat cycle-grid m 1)
     )

    (-
     (groove-metro-beat base-grid m 2)
     (groove-metro-beat displace-grid m 2)
     )
    ))
