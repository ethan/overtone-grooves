(ns groove-step-seq.groove-b
  (:use
   [overtone.core]
   [groove-step-seq.groove-buffer]
   )
  (:require
   [fastmath.core :as m]
   [fastmath.interpolation :as m-i]
   [fastmath.easings :as m-e]
   )
  ;; (:gen-class)
  ;; (:import [javafx.application Platform])
  )

(connect-external-server)
(use 'overtone.inst.drum)
(use 'overtone.inst.synth)

(definst tb303-osc
  [note       {:default 60 :min 0 :max 120 :step 1}
   gate       {:default 0, :min 0 :max 1 :step 1}
   wave       {:default 1 :min 0 :max 2 :step 1}
   slide      {:default 0.1 :min 0 :max 1 :step 0.001}
   r          {:default 0.8 :min 0.01 :max 0.99 :step 0.01}
   attack     {:default 0.01 :min 0.001 :max 4 :step 0.001}
   decay      {:default 0.1 :min 0.001 :max 4 :step 0.001}
   sustain    {:default 0.6 :min 0.001 :max 0.99 :step 0.001}
   release    {:default 0.01 :min 0.001 :max 4 :step 0.001}
   cutoff     {:default 100 :min 1 :max 20000 :step 1}
   env-amount {:default 0.01 :min 0.001 :max 4 :step 0.001}
   amp        {:default 0.5 :min 0 :max 1 :step 0.01}]
  (let [target-freq (midicps note)
        freq       (lag:kr target-freq slide)
        freqs      [freq (* 1.01 freq)]
        vol-env    (env-gen (adsr attack decay sustain release)
                            gate)
        fil-env    (env-gen (perc))
        fil-cutoff (+ cutoff (* env-amount fil-env))
        waves      (* vol-env
                      [(saw freqs)
                       (pulse freqs 0.5)
                       (lf-tri freqs)])
        selector   (select wave waves)
        filt       (rlpf selector fil-cutoff r)]
    ;; TODO Add waveshaper dist and feedback
    (* amp filt)))

(def state (atom {}))

(def bass303 (tb303-osc))
;; (swap! state assoc :bass303 (tb303-osc))
;; (ctl (:bass303 @state) :gate 0)
;; (ctl (:bass303 @state) :gate 1)
(ctl bass303 :gate 1)
(ctl bass303 :note 24)
(ctl bass303 :slide 0.45)
(ctl bass303 :note 32)

(ctl bass303 :amp 2.0)
(ctl bass303 :attack 0.75)
(ctl bass303 :sustain 0.9)

(comment
  (kill bass303))

(def groove1 (groove-metro-synth server-clock-b 104 4 4 1))
(defn groove1a-interpolated-fn [interp-amt]
  (fn [t len]
    (let [interpolation-amount interp-amt
          ;; TODO put these into separate timing synths using ugens
          ;; the speed of the clock
          rate-factor 1.0
          ;; the scale of the interpolators
          scale-factor 1.0
          ;; make period of transform a fraction of the whole
          ;; summed with step function so we still progress through all
          loop-factor (/ 1 2)
          ;; TODO Figure out how to shift window around current time without skipping
          len-limit (* len (/ 1 1))
          offset (* len (/ 0 1))
          rate-scaled-t (+ offset (mod (* t rate-factor) len-limit))
          ;; convert scaled to to 1..scale-factor over length of buffer
          ;; (allows us to more rapidly or slowly run through the output of the func)
          buffer-phase (* rate-scaled-t (/ scale-factor len))
          ]
      (-> buffer-phase
          ;; Loop around loop-factor if we've chosen to
          (mod loop-factor)
          (* (/ 1 loop-factor))
          ;; (+ (* 0.75 1000 (m/sin (* m/PI 2 t (/ 4 9000)))))
          ;; (* (m/sq (/ t 9000)))
          ;; (#(* % (m/smooth-interpolation 0.0 1.0 (/ % 9000))))
          ;; Not sure why we can't just go from 0.0 to len
          ;; (#(* len (m/quad-interpolation 0.0 1.0 (/ % len))))
          
          ;; m/erf ;; has a pleasing sense of hang at the end, esp at higher scales


          ;; Can't quite figure out how to use
          ;; ((partial m/quad-interpolation 0.0 scale-factor))

          ;; Container for trig functions:
          ;; (* m/PI 2)
          ;; (* 2)
          ;; m/sin
          ;; m/cos
          ;; functions that range over -1..1 need to be scaled
          ;; (+ 1.0)
          ;; (/ 2.0)
          ;; ;; protect against crazy bounds
          ;; (max 0)
          ;; (min 10000)

          ;; Older versions of trigs:
          ;; (#(m/sin (* m/PI 1 %)))
          ;; Really nice back and forth feel, like a skate halfpipe, esp at 0.5 interp
          ;; (#(+ 1.0 (m/sin (* m/PI 2 %))))

          ;; ;; Asymptotic funcs
          ;; ;; This doesn't work: 
          ;; (+ 1.0)
          ;; m/tanh
          ;; (+ (m/tanh 1.0))
          ;; (/ (+ 1.0 (m/tanh 1.0)))

          ;; Has a cool skipping feel at 1 beat period
          ;; m/sinh

          ;; Almost a triplet feel at 1 beat
          ;; At 2 beat period it kind of flips around itself, regaeton ish
          ;; Tripletish at 1 beat
          ;; m/log1p

          ;; Pretty slamming at 1 beat period, pause around each beat
          ;; Cool bachata-ish feel at 2 beat period
          ;; Accelerating one bar swing
          m/pow2

          ;; Take off till crazy then land with a thud
          ;; Mechanical martial swing at one beat
          ;; Martial swign at half bar
          ;; Glitchily accelerating one bar swing
          ;; m/pow3

          ;; Train taking off at a moderate pace
          ;; m/sq
          ;; rush slowing down as it goes, moderately
          ;; Sounds good at 1/2 measure
          ;; m/safe-sqrt


          ;; Easings
          ;; m-e/back-in
          ;; m-e/back-in-out

          ;; Bounce is really cool. Would be nice to scale each bounce so tempo
          ;; didn't change drastically and it just felt like bouncing
          ;; Also makes a good 1-2 beat swing, very cowboy
          ;; m-e/bounce-in

          ;; Nice in-out even at full interp
          ;; m-e/quad-in-out
          ;; m-e/quad-out

          ;; m-e/sin-in-out

          ;; m-e/poly-out

          ;; Sounds interesting esp around 0.25 interp
          ;; m-e/cubic-in-out
          ((partial m/wrap 0.0 1.0)
)          (/ scale-factor)
          (* loop-factor)
          (+ (* loop-factor (m/floor (/ buffer-phase loop-factor))))
          ;; Normalize to length of buffer
          (* len)
          ;; Average between linear transformed amount (amt of warp)
          (#(+ (* % interpolation-amount) (* rate-scaled-t (- 1 interpolation-amount))))
          ;; (#(+ (/ % 2) (* t (/ 1 2))))
          )
      )
    )
  )

(def groove1-buffer-a (generate-groove-buffer (groove1a-interpolated-fn 0.25) 104 16))
(write-groove-buffer groove1-buffer-a (groove1a-interpolated-fn 1.0) 104)

(def groover1a (add-groove-transform-synth groove1 groove-clock-shaper groove1-buffer-a))
(comment
  (kill groover1a)
  (kill (:groove-clock-group groove1))
  )

(def pad-notes (vec (chord :A4 :M)))
(def bass-notes (apply concat (mapv vec [(chord :A1 :m7) (chord :A1 :m7 2) (chord :C3 :M7) (chord :A2 :m7 3)])))
(nth bass-notes (rem (quot 34 2.0) 16))
(defn clock-step [step]
  ;; (println step)
  ;; (println (mod step 2))
  (let [
        ;; bing-fn (play-step-amps step play-bing-fn bing-pattern-fn)
        ;; kick-fn (play-step-amps step play-kick2-fn kick-pattern-fn)
        ;; clap-fn (play-step-amps step play-clap-fn clap-pattern-fn)
        bass-step (rem (quot step 16) 16)
        ]
    ;; (if (= 2.0 (mod step 4)) (ctl bass303 :note 48) (ctl bass303 :note 24))
    (ctl bass303 :note (nth bass-notes bass-step))
    (ctl bass303 :gate (case (mod step 4)
                         ;; 0.0 1
                         ;; ;; 2.0 1
                         ;; 3.0 1
                         0))
    (case (mod step 1)
      ;; 0.0 (bing)
      nil
      )
    (case (mod step 4)
      ;; 0.0 (do (kick2) (closed-hat2 0.4))
      ;; 1.0 (closed-hat2 0.3)
      ;; 2.0 (open-hat 0.3 0.15 5000 6000)
      ;; 3.0 (closed-hat2 0.1)
      nil
      )
    (case (mod step 8)
      ;; 4.0 (overtone.inst.drum/snare 1.0)
      nil
      )
    (case (mod step 10)
      ;; 6.0 (mapv #(overpad :amp 0.3 :note %) (mapv note pad-notes))
      nil
      )
    (case (mod step 32)
      0.0 (def pad-notes (vec (chord :C4 :M7)))
      16.0 (def pad-notes (vec (chord :A3 :m7 3)))
      ;; 32.0 (def pad-notes [:A3 :D4 :F4])
      nil
      )
    ;; (bing-fn)
    ;; (kick-fn)
    ;; (clap-fn)
    )
  )
  
  (on-event "/clock-tick"
            ;; #(println "/clock-tick" %)
            (fn [{:keys[args path]}]
              ;; (println args)
              (let [[num clock-id step] args
                    ]
                (cond
                  (= clock-id 1) (clock-step step)
                  ;; (= clock-id 2) (clock-step step)
                  )
                )
              )
            ::clock-tick)
(remove-event-handler ::clock-tick)

(inst-fx! tb303-osc fx-distortion)
(def echo303 (inst-fx! tb303-osc fx-echo ))
(ctl echo303 :delay-time (* (/ 2 104) 60))
(def echo303b (inst-fx! tb303-osc fx-echo ))
(ctl echo303b :delay-time (* (/ 1.5 104) 60))
(inst-fx! tb303-osc fx-reverb)
(clear-fx tb303-osc)
(inst-fx! overpad fx-freeverb)
;; TODO reimplement echo as a tape delay line with heads spawned via step seq
(def echopad (inst-fx! overpad fx-echo))
(def echopad2 (inst-fx! overpad fx-echo))
(ctl echopad :delay-time (* (/ 1 104) 60))
(ctl echopad2 :delay-time (* (/ 0.5 104) 60))
(clear-fx tb303-osc)

(recording-start "~/audio/2021-01-28.A--groove-tb3no3--warp-period-tests.wav")
(recording-stop)
(comment
  (stop)
)

