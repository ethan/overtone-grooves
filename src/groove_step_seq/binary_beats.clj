;; An adaptation of kunstmusic's hex beats using binary notation.
;; This should work for either, but binary is more intuitive for me.
;;
;; (bin->beats 1 0 0 0 , 1 0 0 0 , 0 1 0 0 , 1 0 0 0)
;; (bin->beats 2r1000100001011000)
;; (bin->beats 0x8858)
