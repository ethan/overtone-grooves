(ns groove-step-seq.groove-c
  (:use
   [overtone.core]
   [groove-step-seq.groove-buffer]
   )
  (:require
   [fastmath.core :as m]
   [fastmath.interpolation :as m-i]
   [fastmath.easings :as m-e]
   )
  ;; (:gen-class)
  ;; (:import [javafx.application Platform])
  )

(connect-external-server)
(use 'overtone.inst.drum)
(use 'overtone.inst.synth)

(midi-connected-devices)
(on-event [:midi :note-on]
          (fn [e]
            (fm (midi->hz (:note e)) (/ 4 3) 1.0)
            )
          ::midi-in
          )
(remove-event-handler ::midi-in)
(fm-demo)
(stop)

(defsynth fm [carrier 440 divisor 2.0 depth 1.0 out-bus 0]
  (let [mod-freq (/ carrier divisor)
        mod2-freq (* carrier divisor)
        mod-env   (env-gen (lin 0.5 0 2))
        mod2-env   (env-gen (lin 0.05 0 0.2))
        amp-env   (env-gen (lin 0.01 0.25 0.75) :action FREE)
        modulator2 (* mod-env  (* carrier depth) (sin-osc mod2-freq))
        modulator (* mod-env  (* carrier depth 0.5) (sin-osc mod-freq))
        ]
    (out out-bus (pan2 (* 0.5 amp-env
                          (sin-osc (+ carrier
                                      modulator modulator2)))))))

(groove-buffer-synth-init)

(def groove1 (groove-metro-synth server-clock-b 84 4 4 1))
(defn groove1a-interpolated-fn [interp-amt]
  (fn [t len]
    (let [interpolation-amount interp-amt
          ;; TODO put these into separate timing synths using ugens
          ;; the speed of the clock
          rate-factor 0.5
          ;; the scale of the interpolators
          scale-factor 1.0
          ;; make period of transform a fraction of the whole
          ;; summed with step function so we still progress through all
          loop-period (/ 1 4)
          ;; TODO Figure out how to shift window around current time without skipping
          len-limit (* len (/ 1 1))
          offset (* len (/ 0 1))
          rate-scaled-t (+ offset (mod (* t rate-factor) len-limit))
          ;; convert scaled to to 1..scale-factor over length of buffer
          ;; (allows us to more rapidly or slowly run through the output of the func)
          buffer-phase (* rate-scaled-t (/ scale-factor len))
          ]
      (-> buffer-phase
          ;; Loop around loop-period if we've chosen to
          (mod loop-period)
          (* (/ 1 loop-period))
          ;; (+ (* 0.75 1000 (m/sin (* m/PI 2 t (/ 4 9000)))))
          ;; (* (m/sq (/ t 9000)))
          ;; (#(* % (m/smooth-interpolation 0.0 1.0 (/ % 9000))))
          ;; Not sure why we can't just go from 0.0 to len
          ;; (#(* len (m/quad-interpolation 0.0 1.0 (/ % len))))
          
          ;; m/erf ;; has a pleasing sense of hang at the end, esp at higher scales


          ;; Can't quite figure out how to use
          ;; ((partial m/quad-interpolation 0.0 scale-factor))

          ;; Container for trig functions:
          ;; (* m/PI 2)
          ;; (* 0.5)
          ;; m/sin
          ;; m/cos
          ;; functions that range over -1..1 need to be scaled
          ;; (+ 1.0)
          ;; (/ 2.0)
          ;; ;; protect against crazy bounds
          ;; (max 0)
          ;; (min 10000)

          ;; Older versions of trigs:
          ;; (#(m/sin (* m/PI 1 %)))
          ;; Really nice back and forth feel, like a skate halfpipe, esp at 0.5 interp
          ;; (#(+ 1.0 (m/sin (* m/PI 2 %))))

          ;; ;; Asymptotic funcs
          ;; ;; This doesn't work: 
          ;; (+ 1.0)
          ;; m/tanh
          ;; (+ (m/tanh 1.0))
          ;; (/ (+ 1.0 (m/tanh 1.0)))

          ;; Has a cool skipping feel at 1 beat period
          ;; m/sinh

          ;; Almost a triplet feel at 1 beat
          ;; At 2 beat period it kind of flips around itself, regaeton ish
          ;; Tripletish at 1 beat
          ;; m/log1p

          ;; Pretty slamming at 1 beat period, pause around each beat
          ;; Cool bachata-ish feel at 2 beat period
          ;; Accelerating one bar swing
          ;; m/pow2

          ;; Take off till crazy then land with a thud
          ;; Mechanical martial swing at one beat
          ;; Martial swign at half bar
          ;; Glitchily accelerating one bar swing
          ;; m/pow3

          ;; Train taking off at a moderate pace
          ;; m/sq
          ;; rush slowing down as it goes, moderately
          ;; Sounds good at 1/2 measure
          ;; m/safe-sqrt


          ;; Easings
          ;; m-e/back-in
          ;; m-e/back-in-out

          ;; Bounce is really cool. Would be nice to scale each bounce so tempo
          ;; didn't change drastically and it just felt like bouncing
          ;; Also makes a good 1-2 beat swing, very cowboy
          ;; m-e/bounce-in

          ;; Nice in-out even at full interp
          ;; m-e/quad-in-out
          ;; m-e/quad-out

          ;; m-e/sin-in-out

          ;; m-e/poly-out

          ;; Sounds interesting esp around 0.25 interp
          ;; m-e/cubic-in-out
          ((partial m/wrap 0.0 1.0)
)          (/ scale-factor)
          (* loop-period)
          (+ (* loop-period (m/floor (/ buffer-phase loop-period))))
          ;; Normalize to length of buffer
          (* len)
          ;; Average between linear transformed amount (amt of warp)
          (#(+ (* % interpolation-amount) (* rate-scaled-t (- 1 interpolation-amount))))
          ;; (#(+ (/ % 2) (* t (/ 1 2))))
          )
      )
    )
  )

(def groove1-buffer-a (generate-groove-buffer (groove1a-interpolated-fn 0.25) 104 16))
(write-groove-buffer groove1-buffer-a (groove1a-interpolated-fn 1.0) 104)

(def groover1a (add-groove-transform-synth groove1 groove-clock-shaper groove1-buffer-a))
(comment
  (kill groover1a)
  (kill (:groove-clock-group groove1))
  )

(def pad-notes
  (concat
   (take 12 (cycle (vec (chord :B4 :m))))
   (take 4 (cycle (mapv note [:B4 :D5 :A5 :F#5])))
   (take 12 (cycle (vec (chord :A4 :M))))
   (take 4 (cycle (mapv note [:A4 :C#5 :F#5 :D5])))
   (take 12 (cycle (vec (chord :G4 :M))))
   (take 4 (cycle (mapv note [:G4 :B4 :E5 :C#5])))
   (take 12 (cycle (vec (chord :A4 :M))))
   (take 4 (cycle (mapv note [:A4 :C#5 :F#5 :D5])))
   )
  )
;; (71 74 78 71 74 78 71 74 78 71 74 78 71 74 81 78 69 73 76 69 73 76 69 73 76 69 73 76 69 73 78 74 67 71 74 67 71 74 67 71 74 67 71 74 67 71 76 73 69 73 76 69 73 76 69 73 76 69 73 76 69 73 78 74)
(println pad-notes)

(defn clock-step [step]
  (println step)
  (fm (midi->hz (nth pad-notes (quot step 1))) (/ 4 3) 1.0)
  )

(on-event "/clock-tick"
          ;; #(println "/clock-tick" %)
          (fn [{:keys[args path]}]
            ;; (println args)
            (let [[num clock-id step] args
                  ]
              (cond
                (= clock-id 1) (clock-step step)
                ;; (= clock-id 2) (clock-step step)
                )
              )
            )
          ::clock-tick))
(remove-event-handler ::clock-tick)

(recording-start "~/audio/recordings/2020-01-28--fm-sequence-test.wav")
(recording-stop)
