(ns groove-step-seq.ableton-link
  (:use
   [overtone.core]
   ;; [overtone.live]
   [groove-step-seq.groove-steps])
  (:require [overtone.ableton-link :as link]
            [groove-step-seq.groove-fn :as g-fn]
            )
  )

(connect-external-server)
;; We need to connect to the external server before we can load the insts.
(use 'overtone.inst.drum)
(hat3)
(def steps-per-beat 4)
(def pattern-steps 16)

(link/enable-link true)

;; TODO so cool to make groove by actually manipulating set-bpm instead of our
;; own step count, so it replicated across all clients.
(link/set-bpm 80)

(link/set-quantum (/ pattern-steps steps-per-beat))

(def groove-fn #(-> %
                    ;; ((g-fn/range-rate-compensate 2 0.25 0 1))
                    ((g-fn/range-offset-t 0.25 1 0 2))
                    ((g-fn/range-offset-t 0.25 1 2 4))
                    ))

;; This isn't working as it should. In the test the kicks should be evenly
;; spaced, but arent.
(def groove (g-fn/groove-fn->offset-vec groove-fn 4 4))
;; (def groove-steps (g-fn/groove-fn->beat-vec groove-fn 4 4))

(println groove)

;; TODO create "defpattern" macro that returns step sequence lookup function
(def pattern-hat1 '(
                    1 0.25 0.50 0.30
                    1 0.25 0.50 0.30
                    1 0.25 0.50 0.30
                    1 0.25 0.25 0.30
                    ))

(def pattern-hat2 '(
                    1 0.50 0.90 0.50
                    1 0.50 0.90 0.50
                    1 0.25 0.50 0.30
                    1 0.50 0.90 0.50
                    ))

(def pattern-kick1 '(
                     1.0 0.6 0.4 0.4
                     0.0 0.0 0.0 0.6
                     0.0 0.0 0.0 0.0
                     0.0 0.0 0.0 0.5
                     ))

(def pattern-kick2 '(
                     1.0 0.0 0.0 0.0
                     0.0 0.0 0.0 0.0
                     1.0 0.0 0.0 0.0
                     0.0 0.0 0.0 0.0
                     ))


(def pattern-snare1 '(
                      0.0 0.0 0.0 0.0
                      1.0 0.2 0.3 0.7
                      0.0 0.0 0.0 0.0
                      0.0 0.0 0.0 0.0
                     ))

;; TODO make a function that returns the pattern to ues for any beat (sequence
;; the patterns)
(defn hat-pattern [step] pattern-hat2)
(defn kick-pattern [step] pattern-kick2)
(defn snare-pattern [step] pattern-snare1)

(defn hat-step [step]
  (let [mod-step (mod step pattern-steps)
        pattern (hat-pattern step)
        amp (nth pattern mod-step)
        ]
    (if (> amp 0)
      #(closed-hat :amp (* 0.5 amp) :t 0.03)
      #(fn [])
      )
    )
  )


(defn kick-step [step]
  (let [mod-step (mod step pattern-steps)
        pattern (kick-pattern step)
        amp (nth pattern mod-step)
        ]
    (if (> amp 0)
      #(dance-kick :amp amp)
      #(fn [])
      )
    )
  )

(defn snare-step [step]
  (let [mod-step (mod step pattern-steps)
        pattern (snare-pattern step)
        amp (nth pattern mod-step)
        ]
    (if (> amp 0)
      #(noise-snare :amp amp :freq 3000)
      #(fn [])
      )
    )
  )

;; TODO move this into the groove steps lib.
;; TODO this isn't really needed with new groove-fn approach.
(defn groove-beat-steps [groove pattern-steps steps-per-beat]
  "Return the grid of beat offsets from start of phrase, adjusted by groove."
  (let [beats-per-step (/ 1 steps-per-beat)
        grid (vec (take pattern-steps (iterate #(+ beats-per-step %) 0)))
        grooved-grid (map + grid groove)]
    grooved-grid
    )
  )

;; TODO rewrite this for groove-fn
;; Should take a beat vector instead of a groove (offset) vector.
(defn get-current-beat-groove-step [beat groove]
  (let [grooved-grid (groove-beat-steps groove pattern-steps steps-per-beat)
        beats-per-pattern (/ pattern-steps steps-per-beat)
        ;; TODO should this be phase or beat?
        ;; Currently we're trusting the 0 beat to calculate our own phase. Would
        ;; link do better for us?
        mod-beat (mod beat beats-per-pattern)
        ;; Get all the beats in the grooved grid less than current beat.
        past-beat-list (take-while #(<= % mod-beat) grooved-grid)
        ;; TODO how to handle offsets on the zero beat?
        last-step (- (count past-beat-list) 1)
        ]
    last-step
    )
  )

(defn get-next-groove-beat [beat groove]
  (let [grooved-grid (groove-beat-steps groove pattern-steps steps-per-beat)
        beats-per-pattern (/ pattern-steps steps-per-beat)
        ;; TODO DRY
        ;; TODO should this be phase or beat?
        ;; Currently we're trusting the 0 beat to calculate our own phase. Would
        ;; link do better for us?
        mod-beat (mod beat beats-per-pattern)
        future-beat-list (drop-while #(<= % mod-beat) grooved-grid)
        next-beat-offset (if (empty? future-beat-list)
                           (+ (first groove) beats-per-pattern)
                           (first future-beat-list))
        ;; TODO does this need to have ceil applied?
        pattern-start-beat (- beat mod-beat)
        ]
    (+ pattern-start-beat next-beat-offset)
    )
  )

(defn event-loop
  "Event loop that schedules beats according to a given groove fn."
  [groove-fn step-fn & {:keys [window-start window beats steps-per-beat]
                        :or {window-start (link/get-beat)
                             window 0.25
                             beats 4.0
                             steps-per-beat 4
                             }
                        }]
  (let [window-end (+ window-start window)
        steps (g-fn/groove-fn->beat-vec groove-fn beats steps-per-beat)
        ;; step-beat-paris (g-fn/)
        pattern-window-start (mod beats window-start)
        ;; To ensure that the pattern can end on the start of the next phrase,
        ;; double the length of the window for the mod. This will probably
        ;; break if the window is not an even multiple of the pattern length.
        pattern-window-end (mod (* 2 beats) window-end)
        window-step-times (filter #(and (<= pattern-window-start %) (< % pattern-window-end)) steps)
        ;; The start of the pattern that the window is in.
        ;; TODO determine how this will behave when tempo is changed.
        window-pattern-start (Math/floor (/ window-start beats))
        link-beat (link/get-beat)
        next-window-beat (if (<= link-beat window-end)
                           window-end
                           (/ (Math/ceil (* link-beat window)) window) ;; I think this should find the next window start bigger than the current beat.
                           )
        ]
    (map #(link/at (+ window-pattern-start %) (apply step-fn [%])) window-steps)
    ;; Schedule the next window iteration
    (link/at next-window-beat #(apply #'event-loop [next-window-beat]))
    )
  )

(event-loop groove #(let [step ()
                          snare-fn (snare-step %)
                          kick-fn (kick-step %)
                          ()
                          ]))

;; Currently use a simple clojure to return the groove, allowing us to change it
;; easily (could also just use a namespaced ref with no arguments to event
;; loop).
(event-loop #(do %& groove))
(link/stop-all)

(defn generate-event-loop
  "Event loop generator

  Currently doesn't work, I don't know how to do temporal recursion without
  uisng a constant for the recursed function. Instead, might generate an event
  loop fn name dynamically based on the call and return it.
  "
  ([groove-fn step-fn beats steps-per-beat]
   #(let [beat (link/get-beat)
          ;; groove (groove-fn)
          groove-step (get-current-beat-groove-step beat groove)
          mod-step (mod groove-step pattern-steps)
          next-step-beat (get-next-groove-beat beat groove)
          hat-fn (hat-step mod-step)
          kick-fn (kick-step mod-step)
          snare-fn (snare-step mod-step)
          ]
      (hat-fn) (kick-fn) (snare-fn)
      ;; link/at doesn't appear to take the same form as clojure core at.
      ;; (link/at next-step-beat #'event-loop [groove-fn])
      ;; TODO figure out how to do temporal recursion with multiple event loops
      ;; simulateously (e.g. not all the same defined function and name "event
      ;; loop")
      (link/at next-step-beat #(apply #'event-loop [groove-fn]))
      )
   )
  )
