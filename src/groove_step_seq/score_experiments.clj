; phre ous
(ns groove-step-seq.score-experiments
  (:use
   [overtone.core]
   )
  (:require [overtone.ableton-link :as link]
            [score.core :as score]
            )
  )

(connect-external-server)

(defonce tri-lfo-bus (audio-bus))
(defonce saw-lfo-bus (audio-bus))
(defonce sin-lfo-bus (audio-bus))
(defonce fx-reverb-in (audio-bus 2))

(defonce main-g (group "main"))
(defonce pre-g (group "pre synthesis synths" :head main-g))
(defonce ctl-g (group "control signals" :before pre-g))
(defonce post-g (group "post" :after main-g))

(defsynth tri-lfo-synth [out-bus 0 freq 5]
  (out:kr out-bus (lf-tri:kr freq)))

(defsynth saw-lfo-synth [out-bus 0 freq 5]
  (out:kr out-bus (lf-saw:kr freq)))

(defsynth sin-lfo-synth [out-bus 0 freq 5]
  (out:kr out-bus (sin-osc:kr freq)))

(defsynth panmod-fmod-sin-synth [freq 440 out-bus 0 fmod-bus 0 fmod-amt 0 pan-phasor-bus 0 pan 0 panmod-amt 0 panmod-offset 0]
  ;; TODO re-implement using wave shaping so this can be more versatile.
  "Create a panning sin wave with frequency modulation."
  (let [fmod-in (in:kr fmod-bus)
        freq-with-mod (+ freq (* fmod-in fmod-amt))
        pan-saw-in (in:kr pan-phasor-bus)
        pan-offset-phasor (wrap (+ pan-saw-in panmod-offset) -1.0 1.0)
        ;; TODO look into optimizing this as :kr
        panmod-val (fold (* 2.0 pan-offset-phasor) -1.0 1.0)
        ;; TODO check for out of bounds, limit to -1:1
        pan-with-mod (+ pan (* panmod-val panmod-amt))
        osc-val (sin-osc freq-with-mod)
        ]
    (out out-bus (pan2 osc-val pan-with-mod) )
    )
  )

(comment
  (def tri-lfo (tri-lfo-synth [:tail ctl-g] tri-lfo-bus))
  (def saw-lfo (saw-lfo-synth [:tail ctl-g] saw-lfo-bus))
  (def sin-lfo (sin-lfo-synth [:tail ctl-g] sin-lfo-bus))
  )

(comment
  (def sin-1 (panmod-fmod-sin-synth [:tail main-g] 220.0 0 sin-lfo 10.0 saw-lfo 0.0 1.0 0.0))
  (ctl sin-1 :freq 660)
  (ctl sin-1 :fmod-amt 200)
  (ctl sin-1 :panmod-amt 2)
  (ctl sin-1 :fmod-amt 100)
  )

(comment
  (kill sin-1)
  (stop)
  )
