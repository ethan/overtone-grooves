(ns groove-step-seq.groove-buffer-gui
  (:require
   [cljfx.api :as fx]
   )
  )

(def renderer
  (fx/create-renderer))

(def *state
  (atom {:title "App title" :chart-data (for [i (range 100)]
                                          {:fx/type :xy-chart-data
                                           :x-value i
                                           :y-value (Math/log i)})}))

(def line-chart [{:keys [chart-data]}]
  {:fx/type :line-chart
   :x-axis {:fx/type :number-axis
            :auto-ranging false}
   :y-axis {:fx/type :number-axis
            :auto-ranging false}
   :data [
          {:fx/type :xy-chart-series
           :name "log(x)"
           :data chart-data}
          ]})

(defn root [{:keys [showing]}]
  {:fx/type :stage
   :showing showing
   :scene {:fx/type :scene
           :root {:fx/type :tab-pane
                  :tabs [
                         {:fx/type :tab
                          :text "Line Chart"
                          :closable false
                          :content line-chart}
                         ;; {:fx/type :button
                         ;;  :text "close"
                         ;;  :on-action (fn [_]
                         ;;               (renderer {:fx/type root
                         ;;                          :showing false}))}
                         ]}}})


(renderer {:fx/type root
           :showing true})

(fx/on-fx-thread
 (fx/create-component
  {:fx/type :stage
   :showing true
   :width 960
   :height 540
   :title "Chart Examples"
   :scene {:fx/type :scene
           :root {:fx/type :tab-pane
                  :tabs [
                         {:fx/type :tab
                          :text "Line Chart"
                          :closable false
                          :content line-chart}
                         ]}}}))
