(ns groove-step-seq.groove-fn
  (:use
   [overtone.core]
   ;; [overtone.live]
   [groove-step-seq.groove-steps])
  (:require [overtone.ableton-link :as link]
            [score.core :as score]
            )
  )

;; (connect-external-server)
;; (use 'overtone.inst.drum)

(comment
  ;; The API for composing tempo curves should be something like this
  ;; TODO see if this works for more radical mappings, e.g. reverse time and such.

  ;; This is the newer idea, using fastmath / calcs
  (defn groove-func [beat]
    (-> (linear-infinite beat)
                       ;; call with args for period-length range-start and range-length
                       (apply-func-range-in-period (tanh-offset) 4 1 1)
                       (apply-func-range-in-period (tanh-offset) 4 3 1)
                       ;; default to 0 start and period len
                       (apply-func-range-in-period (tanh-offset) 4)
                       (apply-func (linear-speed-up))
                       ))
  ;; Then call that func with a beat number (starting from 0 at the beginning of
  ;; play):
  (groove-func beat bpm) ;; TODO how to make BPM more variable and less locked
  ;; in under the hood?


  ;; This is the original
  (->> identity
       ;; TODO refactor this as a range-rate-compensate function that determines
       ;; the converse rate change to compensate for a tempo set of a given
       ;; duration.
       (range-rate 1.1 0 0.75)
       (range-rate 0.5 0.75 1)
       (range-with-pause 0.02 1 2) ;; pause for the specified time and then catch up by accelerating tempo
       (range-curvature 1.4 0 2)
       (range-curvature 1.4 2 4)
       (range-offset-t 0.1 0.5 0 1) ;; delay the 0.5 beat by 0.1, stretching before and delaying after
       )
  )

(defn t-between? [t low high]
  (and (<= low t) (> high t))
  )

(defn linear-value
  ([pos rate start val]
   (let [range-pos (- pos start)
         delta (* rate range-pos)
         ]
     (+ val delta)
     )
   )
  ([pos rate start]
   (linear-value pos rate start start)
   )
  )

(defn range-rate
  ([rate offset start end]
   #(if (t-between? % start end)
      (+ offset
         (linear-value % rate start)
       )
      %
      )
   )
  ([rate start end]
   (range-rate rate 0 start end)
   )
  )

(defn range-rate-compensate
  "Adjust the rate for a percent of the range, then compensate for that
  adjustment for the remainder of the range so that the overall tempo is not
  effected by the end."
  [rate rate-range-ratio start end]
  #(let [rate-delta (- rate 1)
         range-len (- end start)
         rate-range-len (* range-len rate-range-ratio)
         rate-range-end (+ start rate-range-len)
         ;; rate-range-fn (range-rate rate start rate-range-end)
         ;; To calculate the offset, we need a range function that spans up to
         ;; and including the end
         rate-full-range-fn (range-rate rate start end)
         rate-range-end-val (rate-full-range-fn rate-range-end)
         ;; Derived from equality of original rate with piecewise function
         ;; composed of initial rate and some compensation rate.
         compensate-offset (/ (* -1 rate-delta rate-range-ratio) (- 1 rate-range-ratio))
         compensate-rate (+ 1 compensate-offset)
         ;; TODO fix this for cases where end-val is greater than end.
         ;; compensate-range-fn (range-rate compensate-rate rate-range-end-val end)
         ]
     (if (< % end)
       (if (<= rate-range-end %)
         (linear-value % compensate-rate rate-range-end rate-range-end-val)
         (if (<= start %)
           (rate-full-range-fn %)
           %
           ))
       %
       )
     )
  )

(defn range-offset-t
  "Offset a moment t in a range, compressing or expanding on either side to
  ensure overall tempo remains constant."
  [offset t start end]
  (let [length-all (- end start)
        length-1 (+ offset t)
        rate-1 (/ length-1 t)
        ratio-1 (/ length-1 length-all)
        ]
    (range-rate-compensate rate-1 ratio-1 start end)
    )
  )

(defn groove-fn->beat-vec
  [fn beats divisions]
  (let [interval (/ 1 divisions)]
    (pick-intervals-to interval beats fn)
    )
  )

(defn groove-fn->interval-vec
  [fn beats divisions]
  (let [interval (/ 1 divisions)
        beat-vec (groove-fn->beat-vec fn beats divisions)
        ]
    (vec (cons 0 (map - (rest beat-vec) beat-vec)))
    )
  )

(defn groove-fn->offset-vec
  [fn beats divisions]
  (let [interval (/ 1 divisions)
        grid-vec (groove-fn->beat-vec identity beats divisions)
        beat-vec (groove-fn->beat-vec fn beats divisions)
        ]
    (mapv - beat-vec grid-vec)
    )
  )

(defn pick-intervals-to
  "Take function values up to a given bound by interval."
  [interval bound fn]
  (vec (map fn (range 0 bound interval)))
  )

(defn sampled-transform-pairs
  "Create sets of input and output values for transform fn."
  [transform-fn inputs]
  (mapv #(vector % (transform-fn %)) inputs)
  )

(defn beat-division-transform-pairs
  "Create a vector of sets of before and after beat values for a give groove
  fn."
  ([groove-fn beats divisions]
   (let [interval (/ 1 divisions)
         grid-vec (groove-fn->beat-vec identity beats divisions)
         ]
     (sampled-transform-pairs groove-fn grid-vec)
     )
   )
  )

(comment
  (def testfn #(-> %
                   ((range-rate-compensate 2 0.25 0 1))
                   ((range-offset-t 0.25 1 2 4))
                   ))
  ;; Create standard 16 step timing vector from func.
  (def test-time-vec (vec (map testfn (range 0 4 0.25))))

  ;; Create an offset vector from the time vecctor
  ;; E.g. each is the offset of each beat from the previous.
  (vec (cons 0 (map - (rest test-time-vec) test-time-vec)))
  (groove-fn->offset-vec testfn 4 4)
  (groove-fn->groove-vec testfn 4 4)
  )

(defn round-ignore
  [val]
  (if (= val (round-down val))
    val
    nil
    )
  )

(defn beat->step
  "Return the step number for a given beat value"
  ([beat divisions rounding-fn]
   (rounding-fn (* beat divisions))
   )
  ([beat divisions]
   (beat->step beat divisions identity)
   )
  )
