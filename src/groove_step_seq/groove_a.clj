(ns groove-step-seq.groove-a
  (:use
   [overtone.core]
   [groove-step-seq.groove-buffer]
   )
  (:require
   [fastmath.core :as m]
   [fastmath.interpolation :as m-i]
   )
  )

(connect-external-server)

;; We need to connect to the external server before we can load the insts.
(use 'overtone.inst.drum)
(use 'overtone.inst.synth)
(use 'overtone.studio.scope)

;; Init the groove-buffer synths
;; TODO add bar number to clock tick
(groove-buffer-synth-init)

(comment 
 (dry-kick)
 (overtone.inst.drum/haziti-clap 200)
 (overtone.inst.drum/noise-snare 880)
 (overtone.inst.drum/bing)
 (tom 220)
 )

(defn play-step-amps [step step-fn pattern-fn]
  (let [mod-step (mod step 16)
        pattern (pattern-fn step)
        step-amp (nth pattern mod-step)
        amp (/ step-amp 10)
        ]
    (if (> amp 0)
      #(step-fn step {:amp amp})
      #(fn [])
      )
    )
  )

(defn bing-pattern-fn [step]
  '(9 5 4 2 9 5 3 1 9 1 3 5 9 5 3 1)
  )

(defn play-bing-fn [step {amp :amp}]
  (bing :amp amp)
  )

(defn kick-pattern-fn [step]
  [
   3 0 1 0
   0 0 0 0
   9 0 0 0
   0 0 0 0
     ]
  ;; (cond
  ;;   (= (quot step 16) 0)
  ;;   [9 0 9 0
  ;;    9 0 0 0
  ;;    9 0 9 0
  ;;    9 0 0 0
  ;;    ]
  ;;   (= (quot step 16) 3)
  ;;   [9 0 0 0
  ;;    9 0 5 0
  ;;    9 0 0 0
  ;;    9 5 9 5
  ;;    ]
  ;;   :else
  ;;   [9 0 0 0
  ;;    0 0 5 0
  ;;    9 0 0 0
  ;;    0 5 0 0
  ;;    ]
  ;;   )
  )

(defn play-kick2-fn [step {amp :amp}]
  (dance-kick :amp amp)
  )

(defn play-kick-fn [step {amp :amp}]
  (dry-kick :amp amp)
  )

(defn clap-pattern-fn [step]
  [0 0 0 0
   9 0 0 0
   9 0 0 0
   1 0 4 0
   ]
  )

(defn play-clap-fn [step {amp :amp}]
  (haziti-clap :amp amp)
  )

(defn clock-buffer-fn [t len]
  t)

(defn groove-buffer-fn [t len]
  (-> t
      ;; (* 1.5)
      ;; (+ (* 0.75 1000 (m/sin (* m/PI 2 t (/ 4 9000)))))
      ;; (* (m/sq (/ t 9000)))
      ;; (#(* % (m/smooth-interpolation 0.0 1.0 (/ % 9000))))
      ;; Not sure why we can't just go from 0.0 to len
      ;; (#(* len (m/quad-interpolation 0.0 1.0 (/ % len))))
      ;; Average between linear the last version
      (#(* len (m/safe-sqrt (/ % len))))
      ;; (#(+ (/ % 4) (* t (/ 3 4))))
      ;; (#(+ (/ % 2) (* t (/ 1 2))))
      )
  )

(def groove-buffer-1 (generate-groove-buffer groove-buffer-fn 80 16))
(write-groove-buffer groove-buffer-1 groove-buffer-fn 80)
(comment
  (buffer-info groove-buffer-1)
  groove-block-bus-1
  )

(def groove-clock-bus-1 (control-bus 1 "Groove Clock Bus 1"))
(def groove-shaper (groove-clock-shaper [:tail (foundation-timing-group)] server-clock-b groove-clock-bus-1 groove-buffer-1))
(def groove-stepper (clock-tick-stepper [:tail (foundation-timing-group)] groove-clock-bus-1 groove-clock-bus-1 80 4))
(def groove-tick-trigger (tick-metro-trigger [:tail (foundation-timing-group)] groove-clock-bus-1 1))

;; TODO redefine all BPM to root of 100?
(defn clock-buffer-fn [t len]
  (let [
        xs-in-beats [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]
        ys-in-amount [0 0.0 0.5 -0.125
                      0 0 -0.5 0.5
                      0 0.5 -0.5 0.25
                      0 0.5 -1 0]
        interpolator (groove-interpolator m-i/b-spline xs-in-beats ys-in-amount 80 0.25)
        ]
    (-> t
        ;; (* 0)
        ;; (* 1.0)
        (interpolator)
        )
    )
  )


(def clock-buffer-1 (generate-groove-buffer clock-buffer-fn 80 16))
(write-groove-buffer clock-buffer-1 clock-buffer-fn 80)
(comment
  (buffer-info clock-buffer-1)
  (buffer-get clock-buffer-1 150)
  )
;; TODO generate spline offset for straight clock groove that pulls 3 back in
;; the pocket.

(def clock-clock-bus-1 (control-bus 1 "Clock Clock Bus 1"))
(def clock-offsetter (groove-clock-offsetter [:tail (foundation-timing-group)] server-clock-b clock-clock-bus-1 clock-buffer-1))
(def clock-stepper (clock-tick-stepper [:tail (foundation-timing-group)] clock-clock-bus-1 clock-clock-bus-1 80 4))
(def clock-tick-trigger (tick-metro-trigger [:tail (foundation-timing-group)] clock-clock-bus-1 2))

(comment
  (kill groove-shaper)
  (kill groove-stepper)
  (kill groove-tick-trigger)
  (kill clock-offsetter)
  (kill clock-stepper)
  (kill clock-tick-trigger)
  )

(defn groove-step [step]
  (let [
        bing-fn (play-step-amps step play-bing-fn bing-pattern-fn)
        kick-fn (play-step-amps step play-kick-fn kick-pattern-fn)
        clap-fn (play-step-amps step play-clap-fn clap-pattern-fn)
        ]
    (bing-fn)
    ;; (kick-fn)
    (clap-fn)
    )
  )

(defn clock-step [step]
  (let [
        bing-fn (play-step-amps step play-bing-fn bing-pattern-fn)
        kick-fn (play-step-amps step play-kick2-fn kick-pattern-fn)
        clap-fn (play-step-amps step play-clap-fn clap-pattern-fn)
        ]
    (bing-fn)
    ;; (kick-fn)
    (clap-fn)
   )
  )

(on-event "/clock-tick"
          ;; #(println "/clock-tick" %)
          ;; TODO figure out how to destructure the packet that comes in with the
          ;; reply
          (fn [{:keys[args path]}]
            ;; (println args)
            (let [[num clock-id step] args
                  ]
              (cond
                (= clock-id 1) (groove-step step)
                (= clock-id 2) (clock-step step)
                )
              )
            )
          ::clock-tick)

(remove-event-handler ::clock-tick)

(require 'overtone.studio.mixer)
(require 'overtone.studio.inst)
(inst-fx! dance-kick fx-reverb)
(overtone.studio.inst/inst-fx! bing fx-reverb)

(inst-pan! bing 0.7)
(inst-fx! bing fx-freeverb)
(inst-pan! haziti-clap -0.7)
(inst-fx! haziti-clap fx-freeverb)
(def haziti-gate 
  (inst-fx! haziti-clap fx-noise-gate))
(ctl haziti-gate :threshold 0.9)

(def kick-krusher 
  (inst-fx! dance-kick fx-distortion))
(def kick-verb
  (inst-fx! dance-kick fx-freeverb)
  )

(clear-fx dance-kick)
(clear-fx haziti-clap)
(clear-fx bing)

(inst-fx! dry-kick fx-reverb)

(recording-start "~/audio/2021-01-24.2-FX-and-offset-groove.wav")
(recording-stop)
