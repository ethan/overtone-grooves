(ns groove-step-seq.core
  (:use
   [overtone.core]
   ;; [overtone.live]
   [groove-step-seq.groove-steps]
   )
  (:require
   [fastmath.core :as m]
   [fastmath.easings :as m-ease]
   )
  )

(connect-external-server)
;; We need to connect to the external server before we can load the insts.
(use 'overtone.inst.drum)
(use 'overtone.inst.synth)
(use 'overtone.studio.scope)

;; TODO create "defpattern" macro that returns step sequence lookup function
(def pattern-hat1 '(
                    0.25 0.25 0.5 0.3
                    0.05 0.25 0.05 0.3
                    0.25 0.25 0.05 0.3
                    0.05 0.25 0.5 0.3
                    ))

(comment
  (def pattern-hat1 '(
                      0.0 0.0 0.25 0.0
                      0.0 0.0 0.5 0.0
                      0.0 0.0 0.25 0.0
                      0.0 0.0 0.5 0.0
                      ))
  )

(def pattern-kick1 '(
                     1.0 0.2 0.4 0.2
                     0.8 0.0 0.5 0.0
                     1.0 0.0 0.5 0.0
                     0.8 0.0 0.4 0.2
                     ))

(comment
  (def pattern-kick1 '(
                       1.0 0.0 0.0 0.0
                       0.8 0.0 0.0 0.0
                       1.0 0.0 0.0 0.0
                       0.8 0.0 0.0 0.0
                       )
    )
  (def pattern-kick1 '(
                       1.0 0.0 0.0 0.0
                       0.8 0.2 0.0 0.0
                       1.0 0.0 0.0 0.0
                       0.8 0.2 0.0 0.1
                       )
    )
  )

(def pattern-snare1 '(
                      0 0 0.0 0
                      1 0 0 0
                      0.0 0 0.5 0
                      1 0.0 0.0 0.0
                     ))

(comment
  (dance-kick :amp 1)
  (def pattern-snare1 '(
                        0 0 0.0 0
                        1 0 0 0
                        0.0 0 0.0 0
                        1 0.0 0.0 0.0
                        ))
  (def pattern-snare1 '(
                        1 0 0.0 0
                        1 0 0.0 0
                        1 0 0.8 0
                        1 0 0.0 0
                        ))
  )

;; TODO make a function that returns the pattern to ues for any beat (sequence
;; the patterns)
(defn hat-pattern [step] pattern-hat1)
(defn kick-pattern [step] pattern-kick1)
(defn snare-pattern [step] pattern-snare1)

(defn hat-step [step]
  (let [mod-step (mod step 16)
        pattern (hat-pattern step)
        amp (nth pattern mod-step)
        ]
    (if (> amp 0)
      #(closed-hat2 :amp amp :decay (* 0.2 amp))
      #(fn [])
      )
    )
  )


(defn kick-step [step]
  (let [mod-step (mod step 16)
        pattern (kick-pattern step)
        amp (nth pattern mod-step)
        ]
    (if (> amp 0)
      #(dance-kick :amp amp :decay 0.1)
      #(fn [])
      )
    )
  )

(defn kick2-step [step]
  (let [mod-step (mod step 16)
        pattern (kick-pattern step)
        amp (nth pattern mod-step)
        ]
    (if (> amp 0)
      #(kick2 :freq 34 :amp amp :decay 0.1)
      #(fn [])
      )
    )
  )
(defn snare-step [step]
  (let [mod-step (mod step 16)
        pattern (snare-pattern step)
        amp (nth pattern mod-step)
        ]
    (if (> amp 0)
      #(noise-snare :amp amp :freq (* 4 440) :decay (* 0.1 (/ 1 amp)))
      #(fn [])
      )
    )
  )

(defn clap-step [step]
  (let [mod-step (mod step 16)
        pattern (snare-pattern step)
        amp (nth pattern mod-step)
        ]
    (if (> amp 0)
      #(clap :amp amp :decay 0.07 )
      #(fn [])
      )
    )
  )

(defn event-loop [m groove-fn t step]
  (let [mod-step (mod step 16)
        next-step (inc step)
        hat-fn (hat-step mod-step)
        kick-fn (kick-step mod-step)
        snare-fn (snare-step mod-step)
        next-time (groove-metro-beat (groove-fn) m (/ next-step 4))
        ]
    (at t
        (hat-fn)
        (kick-fn) (snare-fn)
        )
    (apply-by next-time #'event-loop [m groove-fn next-time next-step])
    )
  )

(defn event-loop2 [m groove-fn t step]
  (let [mod-step (mod step 16)
        next-step (inc step)
        hat-fn (hat-step mod-step)
        kick-fn (kick-step mod-step)
        snare-fn (snare-step mod-step)
        next-time (groove-metro-beat (groove-fn) m (/ next-step 4))
        ]
    (at t
        (hat-fn) 
        )
    (apply-by next-time #'event-loop2 [m groove-fn next-time next-step])
    )
  )

;; TODO figure out how to allow adjusting of the groove live while playing.
;; TODO add quatratic, exponential and log btw points (e.g. curve intersects
;; linear with bend)
(def groove (-> (groove-init 16)
                ;; (groove-set-tempo ,,, 0.86 0 4)
                ;; (groove-set-tempo ,,, 0.89 4 4)
                ;; (groove-set-tempo ,,, 0.92 8 4)
                ;; (groove-set-tempo ,,, 1 12 3)
                (groove-set-tempo ,,, 1.1 0 4)
                (groove-set-tempo ,,, 0.9 4 4)
                (groove-set-tempo ,,, 1.1 8 4)
                (groove-set-tempo ,,, 0.9 12 2)
                (groove-set-tempo ,,, 1.2 14 2)
                (groove-displace-beat ,,, 0.07 12)
                ))

(def groove2 (-> (groove-init 16)
                 (groove-set-tempo ,,, 1.0 0 4)
                 (groove-set-tempo ,,, 1.0 4 4)
                 (groove-set-tempo ,,, 1.05 8 4)
                 (groove-set-tempo ,,, 1.1 12 2)
                 (groove-set-tempo ,,, 1.15 14 2)
                 ))

(comment 
  (def groove (-> (groove-init 16)
                  (groove-set-tempo ,,, 1.66 0 4)
                  (groove-set-tempo ,,, 1.5 4 4)
                  (groove-set-tempo ,,, 0.5 6 6)
                  (groove-set-tempo ,,, 1.0 12 2)
                  (groove-set-tempo ,,, 1.25 14 2)
                  )))

(do
  (def m (metronome 96))
  (event-loop m #(do %& groove) (now) 0)
  (event-loop2 m #(do %& groove2) (now) 0)
  )
(stop)

(recording-start "~/audio/trainwreck-20210120-1-80bpm.wav")
(recording-stop)

;; Explorations for groove-fn
;; Note that the groove-fn curve is inverse: it takes "composition time" and
;; returns "real time". Thus it's the Y axis that is current time, not the X.
;; This is a bit counterintuitive when building functions...curves that rise
;; faster are actually slower (mapping composition time to real times further
;; int he future).
;;
;; The best way to fix this is to use an internal sequencer at control rate that
;; is constantly checking a lookup table (see further down).
(defn groove-fn-metro-beat
  "Get a beat timestamp from a metro filtered through a groove quantization grid."
  [groove-fn m beat-number] ;; TODO do we need to put in -> compatible order, as here?
  (let [
        metro-timestamp-for-beat (m beat-number)
        grooved-timestamp (groove-fn metro-timestamp-for-beat beat-number m)
        ]
    grooved-timestamp
    )
  )

(defn groove-sin-offset [time max-offset period]
  (+ time 
     (* max-offset
        (m/sin (* m/PI 2 (/ time period)))))
  )


(defn groove-fn [time beat-number metro] time
  (let [
        beat-time (metro-tick metro)
        bar-time (metro-tock metro)
        grooved-timestamp (+
                       ;; (* 0.125 (metro-tick metro) (m/sin (* m/PI 2 (/ time (* beat-time 2)))))
                       (groove-sin-offset time (* 0.25 (metro-tick metro)) (* bar-time 16))
                       )
        ]
    ;; (println groove-offset)
    ;; (println grooved-timestamp)
    grooved-timestamp
   )
  )

(defn groove-fn-2 [time beat-number metro] time
  (let [
        beat-time (metro-tick metro)
        bar-time (metro-tock metro)
        grooved-timestamp (-> time 
                           ;; (* 0.125 (metro-tick metro) (m/sin (* m/PI 2 (/ time (* beat-time 2)))))
                           (groove-sin-offset (* -0.125 (metro-tick metro)) (/ bar-time 12))
                           )
        ]
    ;; (println groove-offset)
    ;; (println grooved-timestamp)
    grooved-timestamp
    )
  )

(defn groove-fn-event-loop-1
  ;; Use groove-fn global when not passed as arg (so it can be changed on the fly)
  ([m t step]
   (let [mod-step (mod step 16)
         next-step (inc step)
         hat-fn (hat-step mod-step)
         kick-fn (kick-step mod-step)
         snare-fn (snare-step mod-step)
         next-time (groove-fn-metro-beat groove-fn m (/ next-step 4))
         ]
     (at t
         (hat-fn)
         (kick-fn)
         (snare-fn)
         )
     (apply-by next-time #'groove-fn-event-loop-1 [m next-time next-step]))
   )

     ([m this-groove-fn t step]
     (let [mod-step (mod step 16)
           next-step (inc step)
           hat-fn (hat-step mod-step)
           kick-fn (kick2-step mod-step)
           snare-fn (clap-step mod-step)
           next-time (groove-fn-metro-beat this-groove-fn m (/ next-step 4))
           ]
       (at t
           (hat-fn)
           (kick-fn)
           (snare-fn)
           )
       (apply-by next-time #'groove-fn-event-loop-1 [m this-groove-fn next-time next-step])
       )
      )
  )
  


(do
  (def m (metronome 96))
  ;; (groove-fn-event-loop-1 m groove-fn (now) 0)
  ;; Use global groove-fn for easy development
  (groove-fn-event-loop-1 m (now) 0)
  (groove-fn-event-loop-1 m groove-fn-2 (now) 0)
  (comment
    (metro-bpm m 80)
    )
  )
(stop)

(def saw1 (supersaw :amp 0.25 :freq (midi->hz 34)))
(def saw2 (supersaw :amp 0.25 :freq (midi->hz 39)))
(def saw3 (supersaw :amp 0.25 :freq (midi->hz 45)))

(ctl saw3 :freq (midi->hz 48))
(ctl saw2 :freq (midi->hz 56))

(map #(ctl % :amp 0.30) [saw1 saw2 saw3])

(kill saw1)
(kill saw2)
(kill saw3)

(def bass1 (bass :freq (midi->hz 34) :amp 0.5))
(kill bass1)

;; Explorations for groove-buffer

;; This is the inverse of the groove-fn approach: takes the actual time as input
;; and returns the active step
;;
;; Using a server-side buffer has a bunch of advantages: it can handle jumping
;; around in time, it can also be used as an index into playback buffers (for
;; sample chopping and delays), etc.
;;
;; May need to watch out for maximum float size (2^24)
;;
;; TODO Experiment with using an in-synth clock vs. the overtone clock

;; TODO determine if buffers should be kr or ar (currently trying kr)
(defn write-groove-buffer [groove-buffer groove-buffer-fn]
  (let [
        len (num-frames groove-buffer)
        groove-vector (mapv (fn [n] (groove-buffer-fn n len)) (range 0 len))
        filled-groove-buffer (buffer-write-relay! groove-buffer groove-vector)]
    (println (take 10 groove-vector))
    filled-groove-buffer))

(defn buffer-length-from-bpm-beats [bpm num-beats]
  (let [bps (/ bpm 60)
        spb (/ 1 bps)
        ticks-per-second (server-control-rate)
        length-in-seconds (* num-beats spb)
        ]
    ;; TODO determine if server-control-dir is micro or milliseconds
    ;; Based on
    ;; https://github.com/overtone/overtone/blob/master/src/overtone/sc/clock.clj
    ;; it looks like it's likely microseconds 
    (* ticks-per-second length-in-seconds)
    )
  )

(defn generate-groove-buffer [groove-buffer-fn bpm beats]
  (-> (buffer-length-from-bpm-beats bpm beats)
      (buffer)
      (write-groove-buffer groove-buffer-fn)
      )
  ;; (let [
  ;;       len (buffer-length-from-bpm-beats bpm beats)
  ;;       empty-buffer     (buffer len)
  ;;       groove-buffer (write-groove-buffer empty-buffer groove-buffer-fn)]
  ;;   groove-buffer)
  )

;; TODO extract this into a regular interface
;; TODO implement piecewise functions and compose smaller subdivisions for
;; things like swing
(defn groove-buffer-fn [t len]
  (-> t
      (* 1.0)
      ;; (+ (* 0.75 1000 (m/sin (* m/PI 2 t (/ 4 9000)))))
      ;; (* (m/sq (/ t 9000)))
      ;; (#(* % (m/smooth-interpolation 0.0 1.0 (/ % 9000))))
      ;; Not sure why we can't just go from 0.0 to len
      ;; (#(* len (m/quad-interpolation 0.0 1.0 (/ % len))))
      ;; Average between linear the last version
      ;; (#(+ (/ % 2) (/ t 2)))
      (#(* len (m/safe-sqrt (/ % len))))
      )
  )

(def groove-buffer-1 (generate-groove-buffer groove-buffer-fn 80 16))
(comment 
  (write-groove-buffer groove-buffer-1 groove-buffer-fn))

;; TODO optimize this, it's doing a ton every tick
;; Easiest optimization is probably to do a lot of the pre-compute in the groove
;; buffer
;; Maybe make one groove buffer for beat calcs, and another corresponding one
;; for use as an ar index.
;; TODO Debug why this is throwing an error about the buffern not passing buffer?
(defsynth clock-tick-stepper [clock-bus 0 clock-tick-bus 1 bpm 60 tpb 4 message-key "/clock-tick"]
  (let [
        ;; TODO figure out how to handle 1 vs 2 channel clocks
        ;; E.g. for Overtone internal clock, should get small tick and big tick from the overtone clock
        ;; [s-t b-t] (in:kr clock-bus 2)
        ;; This implementation will hiccup when small-tick wraps around and
        ;; big-tick increments.
        in-clock (in:kr clock-bus)
        seconds-per-tick (server-control-dur)
        clock-seconds (* in-clock seconds-per-tick)
        bps (/ bpm 60)
        spb (/ 1 bps)
        clock-beat-count (/ clock-seconds spb)
        ;; Int ticks (beat subdivisions)
        clock-tick-num (floor (* tpb clock-beat-count))
        ;; Fractional beats
        ;; TODO determine if we should send beats on a separate channel
        ;; clock-beat-num (floor clock-beat-count)
        ;; clock-beat-subdiv (/ clock-tick-num tpb)
        ]
    ;; (send-trig:kr tick-changed-trig tick-num)
    (out:kr clock-tick-bus clock-tick-num)
    )
  )
)

(defsynth tick-metro-trigger [metro-in-bus 0 message-key "/clock-tick" message-idx 1]
  (let [
        tick-num (in:kr metro-in-bus 1)
        tick-changed-trig (absdif tick-num (delay1:kr tick-num))
        ]
    (send-reply:kr tick-changed-trig message-key [tick-num] message-idx)
    )
)

(defsynth groove-clock-shaper [clock-bus 0 groove-clock-bus 1 groove-buffer 1]
  (let [
        ;; First get small tick and big tick from the overtone clock
        in-clock (in:kr clock-bus 1)
        ;; Then pass that through the groove buffer transform
        ;; (returns num ticks mapped, not seconds)
        groove-buffer-frames (buf-frames:kr groove-buffer)
        groove-buffer-index (mod in-clock groove-buffer-frames)
        ;; Read the corresponding buffer value ("waveshaping" the clock)
        groove-result (buf-rd:kr 1 groove-buffer groove-buffer-index 1)
        ;; Now compute the number of seconds that result coresponds to
        ]
    ;; (send-trig:kr tick-changed-trig tick-num)
    ;; (send-reply:kr groove-tick-changed-trig message-key [groove-tick-num groove-beat-subdiv s-t groove-result groove-buffer-index groove-buffer-frames] 1)
    (out:kr groove-clock-bus groove-result)
    )
  )

(defsynth groove-clock-offsetter [clock-bus 0 groove-clock-bus 1 groove-buffer 1 mult 1]
  (let [
        ;; First get small tick and big tick from the overtone clock
        in-clock (in:kr clock-bus 1)
        ;; Then pass that through the groove buffer transform
        ;; (returns num ticks mapped, not seconds)
        groove-buffer-frames (buf-frames:kr groove-buffer)
        ;; TODO may want to make this an arg so we can change the lookup size on
        ;; the fly as tempo changes, etc.
        groove-buffer-index (mod in-clock groove-buffer-frames)
        ;; Read the corresponding buffer value ("waveshaping" the clock)
        groove-buffer-value (buf-rd:kr 1 groove-buffer groove-buffer-index 1)
        ;; Add the offset table value to the clock value, with a multiplier to
        ;; adjust amount of offset.
        groove-result (+ in-clock (* mult groove-buffer-value))
        ]
    ;; (send-trig:kr tick-changed-trig tick-num)
    ;; (send-reply:kr groove-tick-changed-trig message-key [groove-tick-num groove-beat-subdiv s-t groove-result groove-buffer-index groove-buffer-frames] 1)
    (out:kr groove-clock-bus groove-result)
    )
  )

(defsynth groove-tick-stepper [clock-bus 0 groove-clock-bus 1 groove-buffer 1 bpm 60 tpb 4 message-key "/groove-tick"]
  (let [
        ;; First get small tick and big tick from the overtone clock
        [s-t b-t] (in:kr clock-bus 2)
        ;; Then pass that through the groove buffer transform
        ;; (returns num ticks mapped, not seconds)
        ;; TODO Explore using buffer-read with interpolation and
        ;; computed phase
        groove-buffer-frames (buf-frames:kr groove-buffer)
        ;; groove-buffer-frames 45000
        ;; TODO figure out how to also factor in big-ticks
        ;; Currently 
        groove-buffer-index (mod s-t groove-buffer-frames)
        ;; We need to mult by buffer size to get raw value
        ;; TODO see how the float conversion may mess things up, is
        ;; there a way around this?
        groove-result (* 1 (buf-rd:kr 1 groove-buffer groove-buffer-index 1))
        ;; Now compute the number of seconds that result coresponds to
        seconds-per-tick (server-control-dur)
        groove-result-seconds (* seconds-per-tick groove-result)
        ;; Now get the beat num for that number of seconds
        bps (/ bpm 60)
        spb (/ 1 bps)
        groove-beat-count (/ groove-result-seconds spb)
        ;; Subdivisions per beat that make a metro tick (not a clock tick)
        ;; Int ticks (beat subdivisions)
        groove-tick-num (floor (* tpb groove-beat-count))
        ;; Fractional beats
        ;; groove-beat-num (floor groove-beat-count)
        ;; groove-beat-tick-subdiv (/ groove-tick-num tpb)
        groove-tick-changed-trig (absdif groove-tick-num (delay1:kr groove-tick-num))
        ]
    ;; (send-trig:kr tick-changed-trig tick-num)
    ;; (send-reply:kr groove-tick-changed-trig message-key [groove-tick-num groove-beat-subdiv s-t groove-result groove-buffer-index groove-buffer-frames] 1)
    (out:kr groove-clock-bus groove-tick-num)
    )
  )

(def groove-clock-bus-1 (control-bus 1 "Groove Clock Bus 1"))
(def groove-beat-bus-1 (control-bus 1 "Groove Clock Beat 1"))

(def groove-clock (groove-clock-transformer [:tail (foundation-timing-group)] server-clock-b groove-clock-bus-1 groove-buffer-1 80))
(kill groove-clock)
groove-clock-bus-1

(defn groove-1-step [step]
  (let [
        mod-step (mod step 16)
        hat-fn (hat-step mod-step)
        kick-fn (kick2-step mod-step)
        snare-fn (snare-step mod-step)
        bass-step (mod step 4)
        ]
    ;; (at (now)
    ;; (println args)
    (hat-fn)
    (kick-fn)
    (cond
      (= (mod step 4) 2.0) (bass :freq (midi->hz 44) :amp 0.5)
      )
    ;; (if (= bass-step 0) )
    ;; (snare-fn)
    ;; )
    )
  )

(on-event "/groove-1-tick"
          ;; #(println "/clock-tick" %)
         ;; TODO figure out how to destructure the packet that comes in with the
         ;; reply
         (fn [{:keys[args path]}]
           (let [[id num step beat-subdiv small-tick groove-result] args
                 ]
             (groove-1-step step)
           ))
          ::groove-1-tick)

(remove-event-handler ::groove-1-tick)

(defn clock-step [step]
  (let [
        mod-step (mod step 16)
        hat-fn (hat-step mod-step)
        kick-fn (kick-step mod-step)
        snare-fn (snare-step mod-step)
        ]
    ;; (at (now)
    ;; (println args)
    (hat-fn)
    ;; (kick-fn)
    (snare-fn)
    (cond
      (= (mod step 4) 2.0) (bass :freq (midi->hz 39) :amp 0.5)
      )
    ;; )
    )
  )

(on-event "/clock-tick"
          ;; #(println "/clock-tick" %)
          ;; TODO figure out how to destructure the packet that comes in with the
          ;; reply
          (fn [{:keys[args path]}]
            (let [[id num step] args
                  ]
              (clock-step step)
              )
            )
          ::clock-tick)

(remove-event-handler ::clock-tick)

(scope groove-buffer-1)
(buffer-id groove-buffer-1)
(server-clock-drift)
