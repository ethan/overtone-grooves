(defproject groove-step-seq "1.0.0-SNAPSHOT"
  ;; :dependencies [[org.clojure/clojure "1.10.1"]
  ;;                [overtone "0.10.6"]
  ;;                ;; [overtone "0.11.0-SNAPSHOT"]
  ;;                [org.clojure/spec.alpha "0.1.143"]
  ;;                [org.clojure/core.async "0.4.474"]
  ;;                ;; Forcing this to get around a load error in overtone
  ;;                [org.clojure/tools.deps.alpha "0.9.853"]
  ;;                ;; [overtone/scsynth "3.9.3-1"]
  ;;                ;; [overtone/scsynth-extras "3.9.3-1"]
  ;;                [overtone/ableton-link "1.0.0-beta11"]
  ;;                [kunstmusik/score "0.4.0"]
  ;;                [generateme/fastmath "2.0.5"]
  ;;                [cljfx "1.7.13"]
  ;;                ]
  ;; :jvm-opts ["-Djava.library.path=/home/ethan/.nix-profile/lib"]
  :resource-paths [
                   ;; "native/linux/x86_64"
                   "/home/ethan/.nix-profile/lib"
  ;;                  ;; "/nix/store/y0dkjwa2xr1n67x5fakq0088amf4x7vm-user-environment/lib"
                   ]
  ;; :native-path "native"
  :source-paths ["src"]
  :plugins [[lein-tools-deps "0.4.5"]]
  :middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]
  :lein-tools-deps/config {:config-files [:install :user :project]}
  ;; :jvm-opts ^:replace [] 
  )
